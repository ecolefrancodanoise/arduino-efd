#include <SPI.h>
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>

int Lightsensor=A1;
int sensorValue;
String message;
String first;


// OLED display TWI address
#define OLED_ADDR   0x3C

Adafruit_SSD1306 display(-1);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif


// Include the Servo library 
#include <Servo.h> 
// Declare the Servo pin 
int servoPin = 3; 
// Create a servo object 
Servo Servo1; 

void setup(){
  Serial.begin(9600);      // sets the serial port to 9600
     // We need to attach the servo to the used pin number 
   Servo1.attach(servoPin);
  pinMode(Lightsensor, INPUT); 
  // initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();
  display.display();

  // display a pixel in each corner of the screen
  display.drawPixel(0, 0, WHITE);
  display.drawPixel(127, 0, WHITE);
  display.drawPixel(0, 63, WHITE);
  display.drawPixel(127, 63, WHITE);

  // display a line of text
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(17,27);
  display.print("EFD OLED");

  // update display with all of the above graphics
  display.display();
  delay(2000);
  display.clearDisplay();
  display.display();
  
}

void loop()
{
  sensorValue = 1023-analogRead(Lightsensor);       // read analog input pin 0
  Serial.println(sensorValue, DEC);  // prints the value read
    display.clearDisplay();
  display.display();

  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print(sensorValue);
  display.display();
if(sensorValue>=750){
    display.clearDisplay();
  display.display();

  display.setTextSize(3);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print("I Love Light!");
  display.display();
  Servo1.write(30); 
   delay(100); 
   Servo1.write(150); 
   delay(100);    
      Servo1.write(30); 
   delay(100); 
   Servo1.write(150); 
   delay(100);
  Servo1.write(30); 
   delay(100); 
   Servo1.write(150); 
   delay(100);    
      Servo1.write(30); 
   delay(100); 
   Servo1.write(150); 
   delay(100);
  Servo1.write(30); 
   delay(100); 
   Servo1.write(150); 
   delay(100);    
      Servo1.write(30); 
   delay(100); 
   Servo1.write(150); 
   delay(100);   
  delay(1000);
}
if(sensorValue<=50){
    display.clearDisplay();
  display.display();

  display.setTextSize(3);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print("I Need Light!");
  display.display();    
      Servo1.write(30); 
   delay(1000); 
   Servo1.write(180); 
   delay(1000);   
  delay(1000);
}

  delay(500);                        // wait 100ms for next reading
}

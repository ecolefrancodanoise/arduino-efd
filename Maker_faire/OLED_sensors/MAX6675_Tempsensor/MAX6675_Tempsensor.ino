// Include the Servo library 
#include <Servo.h> 
// Declare the Servo pin 
int servoPin = 3; 
// Create a servo object 
Servo Servo1; 

#include "max6675.h"

int ktcSO = 8;
int ktcCS = 9;
int ktcCLK = 10;

MAX6675 ktc(ktcCLK, ktcCS, ktcSO);

//OLED
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>
#define OLED_ADDR   0x3C

int sensorValue;

Adafruit_SSD1306 display(-1);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif
  
void setup() {
  Serial.begin(9600);
     Servo1.attach(servoPin); 
  // initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();
  display.display();

  // display a pixel in each corner of the screen
  display.drawPixel(0, 0, WHITE);
  display.drawPixel(127, 0, WHITE);
  display.drawPixel(0, 63, WHITE);
  display.drawPixel(127, 63, WHITE);

  // display a line of text
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(17,27);
  display.print("EFD OLED");

  // update display with all of the above graphics
  display.display();
  delay(2000);
  display.clearDisplay();
  display.display();
  // give the MAX a little time to settle
}

void loop() {
  // basic readout test
  int Temp = ktc.readCelsius();
   Serial.print("Deg C = "); 
   Serial.println(Temp);
   
     display.clearDisplay();
  display.display();

  display.setTextSize(4);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print(Temp);
    display.print(" C*");
  display.display();
  if(Temp>=33){
       display.clearDisplay();
  display.display();

  display.setTextSize(4);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("So");
  display.print("Warm!");
  display.display();
     Servo1.write(20) ;
   delay(300); 
   Servo1.write(110); 
   delay(300);    
        Servo1.write(20); 
   delay(300); 
   Servo1.write(110); 
   delay(300);
        Servo1.write(20) ;
   delay(300); 
   Servo1.write(110); 
   delay(300);
        Servo1.write(20) ;
   delay(300); 
   Servo1.write(110); 
   delay(300);
        Servo1.write(20) ;
   delay(300); 
   Servo1.write(110); 
   delay(300);
      Servo1.write(50); 
      delay(1000);
  }
    if(Temp<=22){
       display.clearDisplay();
  display.display();

  display.setTextSize(4);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("So");
  display.print("Cold!");
  display.display();
     Servo1.write(70); 
   delay(50); 
   Servo1.write(90); 
   delay(50);    
      Servo1.write(70); 
   delay(50); 
   Servo1.write(90); 
   delay(50);    
      Servo1.write(70); 
   delay(50); 
   Servo1.write(90); 
   delay(50);    
      Servo1.write(70); 
   delay(50); 
   Servo1.write(90); 
   delay(50);    
      Servo1.write(70); 
   delay(50); 
   Servo1.write(90); 
   delay(50);    
      Servo1.write(70); 
   delay(50); 
   Servo1.write(90); 
   delay(50);    
      Servo1.write(70); 
   delay(50); 
   Servo1.write(90); 
   delay(50);    
      Servo1.write(70); 
   delay(50); 
   Servo1.write(90); 
   delay(50);    
      Servo1.write(70); 
   delay(50); 
   Servo1.write(90); 
   delay(50);    
      Servo1.write(70); 
   delay(50); 
   Servo1.write(90); 
   delay(50);    
  }

 
   delay(500);
}


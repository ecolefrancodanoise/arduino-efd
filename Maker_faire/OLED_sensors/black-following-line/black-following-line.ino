int LightsensorA1=A1;
int LightsensorA0=A0;
//int sensorValue; //Right sensor
//int sensorValue2; // Left sensor
#include "olimexcontroller.h"

void setup(){
   Serial.begin(9600);
}

int sensorValue() {
    return 1023-analogRead(LightsensorA1);  // read analog input pin 0
}

int sensorValue2() {
    return 1023-analogRead(LightsensorA0);
}

void loop()
{
  OlimexController motorController(4, 5, 6, 7); 
  motorController.setup();
  motorController.execute('w');
  delay(100);
  motorController.execute('x');
  delay(500);
  Serial.println("sensorValue2");
  Serial.println(sensorValue2());
  Serial.println("sensorValue");
  Serial.println(sensorValue());
  
  while(sensorValue() < 20) {
    motorController.execute('a');
    delay(200);
    motorController.execute('x');
    Serial.println("turning left");
  }
  while(sensorValue2() < 90) {
    motorController.execute('d');
    delay(100);
    motorController.execute('x');
    Serial.println("turning right");
  }

}

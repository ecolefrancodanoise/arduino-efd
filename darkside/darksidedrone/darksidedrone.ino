/*
 * A drone using an Arduino Uno Wifi Rev. 2 using a PID controller (without the I for now)
 * To debug over wifi, connect to drone via its AP:
 *   nc 192.168.4.1 3000
 * 
 * Set up the joystick to send via netcat (extract the channel and value positions, then send):
 *  cat  /dev/input/event20 | stdbuf -o0 xxd -c 24 -ps | stdbuf -o0 cut -c33,34,37,38,41,42 | stdbuf -o0 grep -v "^00" | stdbuf -o0 xxd -r -p | nc 192.168.4.1 3000
 *
 * Motor configuration
 * forward
 *  0(CW)  1(CCW)
 *  3(CCW) 2(CW)
 * back
 */

#include <Servo.h>
#include "WiFiEspAT.h"
#include "MPU6050_6Axis_MotionApps20.h"
#include "BasicLinearAlgebra.h"
using BLA::Matrix;

Matrix <3> operator*(float c, Matrix <3> M) {
    return Matrix <3> ({c*M(0), c*M(1), c*M(2)});
}

const char ssid[] = "efdrobot";
const float Kp = 40; const float Kd = 0.8;
bool isArmed = false;
Servo motors[4];
const int motorPins[] = {5,6,9,10};
const int escBasePulseWidth = 1000;
int8_t ctrlValues[] = {0, 0, 0, 0}; //target yaw, pitch, roll, throttle
WiFiServer tcpServer(3000);
MPU6050 mpu;
WiFiClient client;
Matrix<3> previousGyro;
Matrix <3> accelerations = {0, 0, 0};

void setup() {
    for (int i = 0; i < 4; ++i) {
        motors[i].attach(motorPins[i]);
        motors[i].writeMicroseconds(escBasePulseWidth);
    }
    Wire.begin();
    mpu.initialize();
    mpu.dmpInitialize();
    mpu.CalibrateAccel(6);
    mpu.CalibrateGyro(6);
    mpu.setDMPEnabled(true);
    Serial.begin(115200);
    WiFi.init(Serial);
    WiFi.beginAP(ssid);
    tcpServer.begin();
}

void loop() {
    if (!isArmed) {
        if (ctrlValues[0] < -100 && ctrlValues[1] < -100 && ctrlValues[2] < -100 && ctrlValues[3] < -100) {
            isArmed = true;
        }
    }
    if (WiFiClient newClient = tcpServer.accept()) {
        client = newClient;
    }
    uint8_t tcpRxBuffer[296];
    const int nbBytesRead = client.read(tcpRxBuffer, 296);
    for (int i = 0; i < nbBytesRead; i += 3) {
        ctrlValues[tcpRxBuffer[i+1]] = tcpRxBuffer[i+2];
    }
    uint8_t fifoBuffer[64];
    if (mpu.dmpGetCurrentFIFOPacket(fifoBuffer)) {
        Quaternion q;           // [w, x, y, z]         quaternion container
        VectorFloat gravity;    // [x, y, z]            gravity vector
        float ypr[3];           // [yaw, pitch, roll]   rotate clockwise, pitch nose up, roll rightwards
        VectorInt16 gy;         // [roll, pitch, yaw]   roll rightwards, pitch nose down, rotate CCW
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
        mpu.dmpGetGyro(&gy, fifoBuffer);
        const BLA::Matrix <3> yprMat = {0, ypr[1], ypr[2]}; //yaw should be free and not be penalized
        const BLA::Matrix <3> gyro = {-gy.z, -gy.y, gy.x};
        gyro = 0.6 * gyro + 0.4 * previousGyro; //low-pass filter the derivative
        Matrix <4, 3> errorToAction = {  1, -1, -1,
                                        -1, -1,  1,
                                         1,  1,  1,
                                        -1,  1, -1 };
        Matrix <3> target = {ctrlValues[0], ctrlValues[1], ctrlValues[2]}; //Note: for yaw, joystick control indicates relative change
        Matrix <4> motorPowers = errorToAction * (Kp * yprMat + Kd * gyro - target);
        const int throttle = map(ctrlValues[3], -128, 127, 0, 1000) + escBasePulseWidth;
        if (isArmed) {
            for (int i = 0; i < 4; ++i) {
                motors[i].writeMicroseconds(motorPowers(i) + throttle);
            }
        }
        previousGyro = gyro;
    }
}

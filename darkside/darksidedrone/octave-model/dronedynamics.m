% function [angularAcceleration, angularSpeed, orientation] = dronedynamics(motorSpeed, angularSpeed, angularAcceleration, orientation, noiseLevel, responseMatrix)
%
% A model of how the drone (pitch, roll, yaw, pitch', roll', yaw') evolves
% motorSpeed: the speeds of the four motorSpeed
% angularSpeed: the three angular speeds (pitch', roll' and yaw')
% orientation: [pitch, roll, yaw]
% noiseLevel: the standard deviation of the (white) noise applied to the angular speeds
% responseMatrix: a 4x3 matrix describing how the drone accelerates (pitch'', roll'', yaw'') as a function of the motor speeds
% typically proportional to:
%   1  1 -1
%   1 -1  1
%  -1 -1 -1
%  -1  1  1

function [angularAcceleration, angularSpeed, orientation] = dronedynamics(motorSpeed, angularSpeed, orientation, noiseLevel, responseMatrix, dt, mass)
    angularAcceleration = responseMatrix'*motorSpeed / mass;
    angularSpeed += angularAcceleration * dt + noiseLevel*randn(3,1);
    orientation += angularSpeed * dt;
endfunction

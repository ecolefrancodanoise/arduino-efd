clear
motorPowers = zeros(4,6);
motorSpeeds = zeros(4,6); %in the range -150 to 150
angularAccelerations = zeros(3,6);
angularSpeeds = zeros(3,6); %in the range -180 to 180
orientations = [0 0 0]' * ones(1,6); %in the range -0.3 to 0.3
angularSpeed = zeros(3,1);
angularAcceleration = zeros(3,1);
orientation = [0 1 0]'; %zeros(3,1)
dt = 1/20;
noiseLevel = 0;
%Kp = 10; Kd = 2.0; Ka = 0.0; delayTimeSteps = 0; mass = 6.0;
%Kp = 1000; Kd = 200.0; Ka = 0.0; delayTimeSteps = 0; mass = 600.0;
%Kp = 10; Kd = 2.0; Ka = 0.0; delayTimeSteps = 0; mass = 60.0;
%Kp = 10; Kd = 0.02; Ka = 0.0; delayTimeSteps = 0; mass = 6.0;
%Kp = 10; Kd = 2.0; Ka = 0.0; delayTimeSteps = 3; mass = 6.0;
%Kp = 10; Kd = 20.0; Ka = 0.0; delayTimeSteps = 0; mass = 6.0;
%Kp = 10; Kd = 2.0; Ka = 0.0; delayTimeSteps = 0; mass = 1.0;
%Kp = 10; Kd = 3.0; Ka = 0.0; delayTimeSteps = 0; mass = 1.0;
%Kp = 10; Kd = 2.14; Ka = 0.0; delayTimeSteps = 0; mass = 0.06;
%Kp = 10; Kd = 2.15; Ka = 0.0; delayTimeSteps = 0; mass = 0.06;
%Kp = 10; Kd = 2.16; Ka = 0.0; delayTimeSteps = 0; mass = 0.06;
%Kp = 10; Kd = 2.0; Ka = 0.0; delayTimeSteps = 1; mass = 0.133;
%Kp = 10; Kd = 2.0; Ka = 0.0; delayTimeSteps = 4; mass = 6.0;
%Kp = 10; Kd = 2.0; Ka = 0.0; delayTimeSteps = 5; mass = 6.0;
%Kp = 10; Kd = 20.0; Ka = 0.0; delayTimeSteps = 0; mass = 6.0;
%Kp = 10; Kd = 4.0; Ka = 0.0; delayTimeSteps = 5; mass = 6.0;
%Kp = 10; Kd = 4.0; Ka = 0.0; delayTimeSteps = 5; mass = 6.0; noiseLevel = 0.5;
%Kp = 10; Kd = 10.0; Ka = 0.0; delayTimeSteps = 5; mass = 6.0;
Kp = 10; Kd = 10.0; Ka = 0.0; delayTimeSteps = 5; mass = 6.0; noiseLevel = 0.5;

errorToAction = [1 -1 -1
                -1 -1  1
                 1  1  1
                -1  1 -1];

responseMatrix = errorToAction * inv(errorToAction' * errorToAction);

for iteration = 1:1000,
    motorSpeed = motorPowers(:, end-delayTimeSteps);
    noiseLevel = 0*5e-2;
    [angularAcceleration, angularSpeed, orientation] = dronedynamics(motorSpeed, angularSpeed, orientation, noiseLevel, responseMatrix, dt, mass);
    motorPower = -errorToAction  * (Kp * orientation + Kd * (angularSpeed + noiseLevel*randn(3,1)) + Ka * angularAcceleration );
    motorSpeeds(:,end+1) = motorSpeed;
    angularAccelerations(:,end+1) = angularAcceleration;
    angularSpeeds(:,end+1) = angularSpeed;
    orientations(:,end+1) = orientation;
    motorPowers(:,end+1) = motorPower;
endfor

t = (1:size(orientations,2))*dt;
tau = delayTimeSteps * 1/20;
omega = sqrt(Kp/mass)
alpha = (Kp*tau - Kd) / 2 / mass ;
hold off;
plot(t, orientations(2,:), '.-');
hold on;
plot(t,exp(alpha*t).*cos(omega*t))

legend('angle', 'model1')

stability = Kd / (2 * sqrt(mass*Kp))

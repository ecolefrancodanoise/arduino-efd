#include <Arduino_LSM6DS3.h>

float currentAccelerationSampleRate;
float currentGyroscopeSampleRate;
float xpp, ypp, zpp, xp, yp, zp, x, y, z; // Accelerations are in G (earth gravity)
float pitchp, rollp, yawp, pitch, roll, yaw; // Readings are in degrees/second.
//calibration values
float xpp0 = 0;
float ypp0 = 0;
float zpp0 = 0;
float pitchp0 = 0;
float rollp0 = 0;
float yawp0 = 0;
bool gyroInitialized = false;
//control matrix:
// 4 rows: motors FL, FR, RR, RL
// 6 PID errors: acc_x, acc_y, acc_z, pitch, roll, yaw
// x is transverse positive left, y is longitudinal positive backwards
float controlMatrix[4][6] = {
    {-1, -1, +1, -1, -1, +1},
    {+1, -1, +1, -1, +1, -1},
    {+1, +1, +1, +1, +1, +1},
    {-1, +1, +1, +1, -1, -1}
};

void setup() {
    currentAccelerationSampleRate = IMU.accelerationSampleRate();
    currentGyroscopeSampleRate = IMU.gyroscopeSampleRate();
    Serial.begin(115200);
    if (!IMU.begin()) {
        Serial.println("Failed to initialize IMU!");
    }
    delay(3000);

    int nbAccelSamples = 0;
    int nbGyroSamples0 = 0;
    for (int i = 0; i < 1000; ++i) {
        if (IMU.accelerationAvailable()) {
            IMU.readAcceleration(xpp, ypp, zpp);
            xpp0 += xpp;
            ypp0 += ypp;
            zpp0 += zpp;
            nbAccelSamples++;
        }
        if (IMU.gyroscopeAvailable()) {
            IMU.readGyroscope(pitchp, rollp, yawp);
            pitchp0 += pitchp;
            yawp0 += yawp;
            rollp0 += rollp;
            nbGyroSamples0++;
        }
        delay(3);
    }
    xpp0 /= nbAccelSamples;
    ypp0 /= nbAccelSamples;
    zpp0 /= nbAccelSamples;
    pitchp0 /= nbGyroSamples0;
    yawp0 /= nbGyroSamples0;
    rollp0 /= nbGyroSamples0;
    Serial.println(nbAccelSamples);
    Serial.println(currentAccelerationSampleRate);
    Serial.println(currentGyroscopeSampleRate);
}

void loop() {

    if (IMU.gyroscopeAvailable()) {
        IMU.readGyroscope(pitchp, rollp, yawp);
        pitch += (pitchp - pitchp0) / currentGyroscopeSampleRate;
        roll += (rollp - rollp0) / currentGyroscopeSampleRate;
        yaw += (yawp - yawp0) / currentGyroscopeSampleRate;
        Serial.print(pitch);
        Serial.print('\t');
        Serial.print(roll);
        Serial.print('\t');
        Serial.println(yaw);
    }
    if (IMU.accelerationAvailable()) {
        IMU.readAcceleration(xpp, ypp, zpp);
        //Serial.println("Acceleration:");
        Serial.print(xpp - xpp0);
        Serial.print('\t');
        Serial.print(ypp - ypp0);
        Serial.print('\t');
        Serial.println(zpp- zpp0);
    }

}

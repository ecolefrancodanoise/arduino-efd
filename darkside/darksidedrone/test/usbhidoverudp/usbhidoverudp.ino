/*
 * Test sending USB HID joystick packets over UDP
 * 
 * Useful CLI commands:
 *
 * ls -lrt  /dev/input/by-path/
 * cat  /dev/input/event20 >short-yaw
 * hexdump -e '24/1 "%02x " "\n"' short-yaw
 *
 * Set up the joystick to send via UDP:
 * cat  /dev/input/event20 | nc -u 192.168.43.79 3000
 * 
 * Get feedback over UDP using
 *   nc -l -u -p 9999
 */

#include <WiFiNINA.h>
#include <WiFiUdp.h>
const char ssid[] = "Jolly 1";
const char password[] = "aaaaaaaa";
const IPAddress serverIp(192,168,43,112);
const int serverPortNumber = 9999;
WiFiUDP udp;
long int timer = 0;

void setup() {
    Serial.begin(115200);
    WiFi.begin(ssid, password);
    udp.begin(3000);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
}

void loop() {
    if (millis() - timer > 100) {
        udp.beginPacket(serverIp, serverPortNumber);
        udp.write("Alive\n", strlen("Alive\n"));
        udp.endPacket();
        timer = millis();
    }
    digitalWrite(LED_BUILTIN, (millis() / 1000) % 2);
    if(WiFi.status() != WL_CONNECTED) {
        Serial.println("Connection lost. Reconnecting.");
        if(WiFi.begin(ssid, password) == WL_CONNECTED) {
            Serial.print("IP address: ");
            udp.begin(3000);
            Serial.println(WiFi.localIP());
        } else {
            Serial.println("Connection failed");
        }
    }
    if(udp.parsePacket() > 0) {
        uint8_t udpReceiveBuffer[48]; //HID packets appear to be 48 bytes long
        udp.read(udpReceiveBuffer, 48);
        const unsigned int counter1 = (udpReceiveBuffer[9] << 0) | (udpReceiveBuffer[10] << 8);
        const unsigned int counter2 = (udpReceiveBuffer[9+24] << 0) | (udpReceiveBuffer[10+24] << 8);
        const int enabled1 = (int8_t)udpReceiveBuffer[16];
        const int enabled2 = (int8_t)udpReceiveBuffer[16+24];
        if (enabled1) {
            char debugString[70];
            sprintf(debugString, "Counter1: %u, Channel: %d, Value: %d\n", counter1, (int8_t)udpReceiveBuffer[18], (int8_t)udpReceiveBuffer[20]);
            udp.beginPacket(serverIp, serverPortNumber);
            udp.write(debugString, strlen(debugString));
            udp.endPacket();
            Serial.print(debugString);
        }
        if (enabled2) {
            char debugString[70];
            sprintf(debugString, "Counter2: %u, Channel: %d, Value: %d\n", counter2, (int8_t)udpReceiveBuffer[18+24], (int8_t)udpReceiveBuffer[20+24]);
            udp.beginPacket(serverIp, serverPortNumber);
            udp.write(debugString, strlen(debugString));
            udp.endPacket();
            Serial.print(debugString);
        }
        timer = millis();
    }
}

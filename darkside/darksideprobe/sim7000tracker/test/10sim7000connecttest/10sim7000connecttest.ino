#include "HttpStreamClient.h"
#include "wificonfig.h"
#include "Sim7000Client.h"
#include "SoftwareSerial.h"

/*
 * Testing that the GSM modem gets connected to the network
 * Note: getting onto the GSM network can take several minutes, during which
 * this test will return a failure (rapid blinking).
 */

SoftwareSerial* ss = new SoftwareSerial(8,9);
Sim7000Client tcpClient(ss);
HttpStreamClient httpClient(&tcpClient);

const char* urlPath = "/test";
bool connectionOk;

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    Serial.begin(9600);
    ss->begin(9600); //FIXME: get this working at 115000
    connectionOk = tcpClient.begin(PINNUMBER);
}

void loop() {
    const int halfPeriod = connectionOk? 1000 : 200;
    if ((millis() / halfPeriod) % 2 == 0) {
        digitalWrite(LED_BUILTIN, HIGH);
    } else {
        digitalWrite(LED_BUILTIN, LOW);
    }
}

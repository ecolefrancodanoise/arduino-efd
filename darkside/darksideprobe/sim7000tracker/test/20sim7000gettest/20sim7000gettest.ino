#include "HttpStreamClient.h"
#include "wificonfig.h"
#include "Sim7000Client.h"
#include "SoftwareSerial.h"

SoftwareSerial* ss = new SoftwareSerial(8,9);
//HardwareSerial* ss = &Serial;
Sim7000Client tcpClient(ss);
HttpStreamClient httpClient(&tcpClient);

const char* urlPath = "/test";
const char* testText = "123";
bool testSucceeded = false;
const char* testResponse;


void setup() {
    Serial.begin(9600);
    ss->begin(9600); //FIXME: get this working at 115200
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    const bool connectionOk = tcpClient.begin(PINNUMBER);
    if (connectionOk) {
        testResponse = httpClient.get(commandServer, urlPath, 80);
        if (testResponse[0] != 0x0) {
            testSucceeded = strcmp(testResponse, testText);
        }
        Serial.println("Response:");
        Serial.println(testResponse);
    }
}

void loop() {
    const int halfPeriod = testSucceeded? 1000 : 200;
    if ((millis() / halfPeriod) % 2 == 0) {
        digitalWrite(LED_BUILTIN, HIGH);
    } else {
        digitalWrite(LED_BUILTIN, LOW);
    }
}

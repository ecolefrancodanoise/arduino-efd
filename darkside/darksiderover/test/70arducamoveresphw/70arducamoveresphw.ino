#include "HttpStreamClient.h"
#include "ArduCamStream.h"
#include "wificonfig.h"

#ifdef ARDUINO_SAMD_MKRGSM1400
    #include <MKRGSM.h>
    HardwareSerial* ss = &Serial1;
    GSMClient tcpClient;
    GPRS gprs;
    GSM gsmAccess;
#elif ARDUINO_SAMD_MKRNB1500
    #include <MKRGSM.h>
    HardwareSerial* ss = &Serial1;
    NBClient tcpClient;
    GPRS gprs;
    NB nbAccess;
#elif defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
    #include <WiFiNINA.h>
    WiFiClient tcpClient;
#else
    #include "WiFiEspAT.h"
    WiFiClient tcpClient;
#endif

HttpStreamClient httpClient(&tcpClient);
ArduCamStream cameraStream;
#ifdef ARDUINO_ARCH_SAMD
const int CAM_CS_PIN = 7;
#else
const int CAM_CS_PIN = 10;
#endif
bool cameraOk = false;
bool connectionOK = false;
bool captureOk = false;
const char* result;

void setup() {
    const int start = millis();
    Serial.begin(115200);
    #ifdef ARDUINO_SAMD_MKRGSM1400
    connectionOK = (gsmAccess.begin(PINNUMBER) == GSM_READY) &&
                    (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY);
    #elif ARDUINO_SAMD_MKRNB1500
    connectionOK = (nbAccess.begin(PINNUMBER) == NB_READY) &&
                    (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY);
    #elif defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
    connectionOK = WiFi.begin(ssid, password) == WL_CONNECTED;
    #else
    WiFi.init(Serial);
    connectionOK = WiFi.begin(ssid, password) == WL_CONNECTED;
    #endif
    cameraOk = cameraStream.begin(CAM_CS_PIN);
    if(cameraOk) {
        captureOk = cameraStream.capture();
    }
    if (connectionOK && captureOk) {
        result = httpClient.postFile(commandServer, "/write/photo/camtest", commandServerPort, &cameraStream, cameraStream.fifoSize());
    }
}

void loop() {
    const int halfPeriod = captureOk? 1000 : 200;
    if ((millis() / halfPeriod) % 2 == 1) {
        digitalWrite(LED_BUILTIN, HIGH);
    } else {
        digitalWrite(LED_BUILTIN, LOW);
    }
}

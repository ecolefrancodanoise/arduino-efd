/**
 * Test to see if the motor controller and the motors are properly connected
 */

#include "olimexcontroller.h"

void setup() {
    OlimexController motorController(4, 5, 6, 7);
    motorController.setup();
    motorController.execute('w');
    delay(100);
    motorController.execute('a');
    delay(100);
    motorController.execute('s');
    delay(100);
    motorController.execute('d');
    delay(100);
    motorController.execute('x');
}

void loop() {}

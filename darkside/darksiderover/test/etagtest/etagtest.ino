#include "HttpStreamClient.h"
#include "wificonfig.h"
#include "StringStream.h"
#include "WiFiEspAT.h"

/**
 * A test to ensure that ETag logic works as expected
 */

void setup() {
    WiFiClient tcpClient;
    HttpStreamClient httpClient(&tcpClient);
    Serial.begin(115200);
    WiFi.init(Serial);
    bool connectionOK = false;
    if (!connectionOK) {
        connectionOK = WiFi.begin(ssid, password) == WL_CONNECTED;
    }
    for (int i = 0; i <2; ++i) {
        StringStream testStream;
        const char* testString = "test01";
        testStream.setString(testString);
        char postRequest[50];
        sprintf_P(postRequest, PSTR("/%s/test"), robotId);
        httpClient.postFile(commandServer, postRequest, commandServerPort, &testStream, strlen(testString)); //ensure there is gettable data
        char getRequest[50];
        sprintf_P(getRequest, PSTR("/%s/test/-1"), robotId); //get latest command, -1
        const char* httpBody = httpClient.get(commandServer, getRequest, commandServerPort);
        char httpBodyPersistent[20];
        strcpy(httpBodyPersistent, httpBody);
        const char* httpBodyEmpty = httpClient.get(commandServer, getRequest, commandServerPort); //Second GET should be empty (ETag)
        StringStream testResultStream;
        char testResultString[100];
        sprintf_P(testResultString, PSTR("Result:\nFirst Request(test01):%s\nSecond Request(empty):%s\n"), httpBodyPersistent, httpBodyEmpty);
        testResultStream.setString(testResultString);
        httpClient.postFile(commandServer, postRequest, commandServerPort, &testResultStream, strlen(testResultString)); //ensure there is gettable data
    }
}

void loop() {}

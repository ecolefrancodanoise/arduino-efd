
Make sure to sudo apt-get install maven default-jdk

To run the server:
mvn spring-boot:run


```
keytool -genkeypair -alias darkside-https -keyalg RSA -storetype PKCS12 -keystore darkside-https.p12 -storepass your_keystore_password_here #https self-issued certificate
mkdir src/main/resources/certificates
mv darkside-https.p12 src/main/resources/certificates
```

To use an existing SSL certificate (convert from .pem):
```
openssl pkcs12 -export -name "faircert-https" -out faircert-https.p12 -in fullchain.pem -inkey privkey.pem
```

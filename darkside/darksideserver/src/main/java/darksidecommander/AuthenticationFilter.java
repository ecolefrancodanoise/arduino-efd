package darksidecommander;

import org.springframework.web.filter.OncePerRequestFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import org.springframework.stereotype.Component;
import java.io.IOException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import io.jsonwebtoken.security.Jwk;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;


import java.util.Arrays;
/*
 * Class impplementing the core idea of authentication/authorization of the system:
 * Authorization is granted by chaining JWS's in a /api/entitiId/control endpoint. The
 * latest JWS contains the public key (a JWK in sub_jwk) that allows writing to the /api/entitiId
 * endpoint.
 */

@Component
public class AuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    StorageService storageService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        User.UserBuilder userBuilder = User.withUsername("dummy");
        userBuilder.password("slask");
        final String requestStrings[] = request.getRequestURI().split("\\/");
        if (requestStrings.length > 1) {
            final String entityId = requestStrings[1];
            DataSample controlTokenDataSample = storageService.retrieve(entityId, "control", -1);
            if (controlTokenDataSample == null) {
                //no control, all endpoints are open for POST'ing
                userBuilder.authorities("ROLE_POST");
            } else {
                //DataSample should have a public key, check header bearer token signature
                ControlJwt controlJwt = new ControlJwt(new String(controlTokenDataSample.rawContent, "US-ASCII"));
                Jwk subjectJWK = controlJwt.getSubjectJwk();

                String bearerTokenHeader = request.getHeader("Authorization");
                if (bearerTokenHeader != null && bearerTokenHeader.split(" ")[0].equals("Bearer")) {
                    final String bearerToken = bearerTokenHeader.split(" ")[1];
                    try {
                        Jwts.parser()
                            .verifyWith((java.security.PublicKey)subjectJWK.toKey())
                            .build()
                            .parseClaimsJws(bearerToken);
                        userBuilder.authorities("ROLE_POST");
                    } catch (Exception e) {
                        //do nothing
                    }
                }
            }
        }
        UserDetails userDetails = userBuilder.build();
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
        filterChain.doFilter(request, response);
    }
}

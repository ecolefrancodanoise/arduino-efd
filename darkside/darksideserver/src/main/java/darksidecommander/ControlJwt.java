package darksidecommander;

import java.security.spec.KeySpec;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Claims;
import java.security.PublicKey;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.jsonwebtoken.security.Jwk;
import io.jsonwebtoken.security.Jwks;
import java.util.Base64;

/*
 * A class representing the Json Web Token used for controlling access to an entity
 * Typically a self-issued ID token,
 * https://openid.net/specs/openid-connect-self-issued-v2-1_0.html#siop-id-token-validation
 */

public class ControlJwt {

    final public String jwtString;

    public ControlJwt(String jwtString_) {
        jwtString = jwtString_;
    }

    public Claims getClaims(PublicKey keySpec) throws Exception {
        JwtParser parser = Jwts.parser()
            .verifyWith(keySpec)
            .build();
        return parser.parseClaimsJws(jwtString).getPayload();
    }

    /*
     * Retrieve the sub_jwk without validating the JWT
     * Manual workaround due to JJWT being very strict
     */
    public Jwk getSubjectJwk() throws JsonProcessingException {
        String[] chunks = jwtString.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();
        String payload = new String(decoder.decode(chunks[1]));
        ObjectMapper mapper = new ObjectMapper();
        JsonNode content = mapper.readTree(payload);
        String jwkAsString  = mapper.writeValueAsString(content.get("sub_jwk"));
        return Jwks.parser().build().parse(jwkAsString);
    }
}

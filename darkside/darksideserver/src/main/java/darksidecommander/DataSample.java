package darksidecommander;

import java.time.Instant;
import lombok.AllArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;

/*
 * Class allowing to return the sample (which can be raw binary data) with its timestamp,
 * the latter being used e.g. to produce an ETag hash.
 */

@AllArgsConstructor
public class DataSample {

    final int index;

    final byte[] rawContent;

    final Instant lastModified;

    String hash() {
        return DigestUtils.sha256Hex(new String(rawContent) + lastModified.hashCode() + index);
    }
}

package darksidecommander;

import java.io.IOException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.beans.factory.annotation.Autowired;
import java.lang.Deprecated;

@RestController
@EnableMethodSecurity
@CrossOrigin
public class DataSampleController {

    @Autowired
    private StorageService storageService;

    @GetMapping(value = {"/{robotId:(?!static).*}/{dataSampleType}/{requestId}"})
    public ResponseEntity<byte[]> readDataSample(@PathVariable String robotId,
                              @PathVariable Integer requestId, @PathVariable String dataSampleType) throws IOException {
        DataSample dataSample = storageService.retrieve(robotId, dataSampleType, requestId);
        if (dataSample == null) {
            return ResponseEntity.notFound().build();
        }
        String lastModifiedHash = dataSample.hash().substring(0,5);
        return ResponseEntity.ok().eTag(lastModifiedHash).body(dataSample.rawContent);
    }

    @Deprecated
    @PreAuthorize("hasRole('ROLE_POST')")
    @PostMapping(value = {"/{robotId:(?!static).*}/{dataSampleType}"}, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String writeDataSample(@PathVariable String robotId, @PathVariable String dataSampleType, @RequestPart("file") MultipartFile content) throws IOException {
        return storageService.store(robotId, dataSampleType, content.getBytes());
    }

    @PreAuthorize("hasRole('ROLE_POST')")
    @PostMapping("/{robotId:(?!static).*}/{dataSampleType}")
    public String writeDataSampleSimple(@PathVariable String robotId, @PathVariable String dataSampleType, @RequestBody byte[] content) throws IOException {
        return storageService.store(robotId, dataSampleType, content);
    }
}

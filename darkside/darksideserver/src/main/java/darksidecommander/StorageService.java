package darksidecommander;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.time.Instant;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import org.springframework.stereotype.Repository;
import org.springframework.beans.factory.annotation.Value;

/*
 * A class managing storage and retrieval of the incoming resp. outgoing data.
 *
 * The elements are stored and indexed in order of creation. When retrieving an element
 * using a negative index -N, the operation is defined as retrieving the N'th latest element.
 * Specifically, N=-1 means retrieving the newest element.
 *
 */

@Repository
public class StorageService {

    @Value("${rootdir}")
    private String rootDir;

    public String store(String robotId, String dataSampleType, byte[] bs) throws IOException {
        final Integer lastFileName = getLastFilename(dataSampleType, robotId);
        final String newFileName = lastFileName == null? "0" : "" + (lastFileName + 1);
        Files.createDirectories(Paths.get(rootDir + "/" + robotId + "/" + dataSampleType));
        Files.write(Paths.get(rootDir + "/" + robotId + "/" + dataSampleType + "/" + newFileName), bs);
        return newFileName;
    }

    DataSample retrieve(String robotId, String dataSampleType, Integer requestId) throws IOException {
        Integer fileName;
        if(requestId >= 0) {
            fileName = requestId;
        } else {
            Integer lastFileName = getLastFilename(dataSampleType, robotId);
            if (lastFileName == null) {
                return null;
            }
            fileName = lastFileName + requestId + 1; //latest entry is -1
        }
        try {
            final byte[] rawContent = Files.readAllBytes(Paths.get(rootDir + "/" + robotId + "/" + dataSampleType + "/" + fileName));
            final Instant lastModified = Files.getLastModifiedTime(Paths.get(rootDir + "/" + robotId + "/" + dataSampleType + "/" + fileName)).toInstant();
            return new DataSample(fileName, rawContent, lastModified);
        } catch (NoSuchFileException e) {
            return null;
        }
    }

    private Integer getLastFilename(String dataSampleType, String robotId) {
        final String[] fileList = new File(rootDir + "/" + robotId + "/" + dataSampleType).list();
        if (fileList != null && fileList.length > 0) {
            return  Integer.parseInt(Arrays.asList(fileList).parallelStream()
                    .max(Comparator.comparing(s -> Integer.parseInt(s))).get());
        } else {
            return null;
        }
    }
}

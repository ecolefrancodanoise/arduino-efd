package darksidecommander;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = { "rootdir=/tmp/darksidetest/${random.uuid}" })
@AutoConfigureMockMvc
@ContextConfiguration(classes = { WebConfiguration.class, StorageService.class, DataSampleController.class, AuthenticationFilter.class, SecurityConfiguration.class, AuthenticationFilter.class })

public class AuthenticationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testSecureEndpointWithBearerToken() throws Exception {
        final String controlToken = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ5akNCQ0djZ2xGYWZqdHpKbXRWZlY3Z0JaTklKaVVCYWlmbUZKV1FLal9JIiwic3ViIjoieWpDQkNHY2dsRmFmanR6Sm10VmZWN2dCWk5JSmlVQmFpZm1GSldRS2pfSSIsInNjb3BlIjp7ImRvY3VtZW50IjoiZXF4TUNJVFN5SmE1Qy9RaU90ZmxmQndDNVltdEZTM3A4YjdjNzJSd3ptST0ifSwiaWF0IjoxNTE2MjM5MDIyLCJzdWJfandrIjp7Imt0eSI6IlJTQSIsIm4iOiJsS3pHXzdNTlJ6aDBjSkVXaDVhbU9fMldDV1ZaeE1mMFlRTGZwZEZkZzI1Vm41RDdFRXg1UUN1WjBmT2lEdkJtWS00QTd1RWlYMXNnNkRWbE1aRE82S1QzTy0tbkcwaFJKZ1NxZjladVRUWWItSnBhdnZtRjA1dmJBalQyblU2bmw4a1Nhd2t5OF9weEYyYjhBUDNWVXNWRnQtOUZpVC10WG02LUJaMTdYMFFUbXJUZzdlWUxiZWgzQzFmZkhxYkdFLURtck4yWnJXNVN0TUdhdFNORXVGZm1Zb2h5RW4zTWc3TFRvb2ZDODRSYk9FTnNkX2hXaXBmaXI2MFdkTnBwUW84MGpDSzlHOHdmR3RSSlFZdFFvbDFZazd5Zm5MR0NmYU0tM2lkZGtXTWVQNTlSZ1JvcGhraFJ1emZDb1RRbVdkZWJLRnV2NVZ4TlU2d2FWZjQ2eFEiLCJlIjoiQVFBQiJ9fQ.OHKuI8If4eoC5ziFXUk17Do3eYy090Ejs_DPHuWIxVjUdgojpUs3OAkZiJt7uNS9HC_Xq6mTX5x9rM_7bDMNKI2wYQ9A2EfVKj9WCawgcH8cn__voWvCEvg2e0xabOhPsmrAJNemsbV8rV5oDogt5wtATapeg2hl251hPsuT1VetIDOqd5D23oDz9jBbLf8Vw6bqSmDuexAJeMMGI8iWXN39xi7vmLXpN1j4XkzDa23XFEXolBAi6lwhRw0jJX7RhH9A2wVamwhrCr3nqAKVIUUYzdSJOplO5cb0VADgX0MLrv1vC1kW1DZSlwNRvrPXlnMUDprJ0BJpgvjtojdtgA";

        final String authenticationToken = controlToken;

        MockMultipartFile file = new MockMultipartFile(
            "file",
            "hello.txt",
            MediaType.TEXT_PLAIN_VALUE,
            controlToken.getBytes()
        );

        mockMvc.perform(MockMvcRequestBuilders.multipart("/robot-2/control")
                        .file(file))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.get("/robot-2/control/-1"))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/robot-2/control")
                        .file(file)
                        .header("Authorization", "Bearer " + authenticationToken))
                .andExpect(status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.multipart("/robot-2/commands")
                        .file(file))
                .andExpect(status().isForbidden());
    }
}

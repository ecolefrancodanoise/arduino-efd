package darkside;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import org.springframework.test.context.ContextConfiguration;
import static org.junit.jupiter.api.Assertions.assertEquals;
import darksidecommander.ControlJwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.security.Jwks;
import io.jsonwebtoken.security.Jwk;
import io.jsonwebtoken.security.JwkThumbprint;

import java.util.LinkedHashMap;

@ContextConfiguration(classes = { ControlJwt.class })
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ControlJwtTest {

    final String jwt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ5akNCQ0djZ2xGYWZqdHpKbXRWZlY3Z0JaTklKaVVCYWlmbUZKV1FLal9JIiwic3ViIjoieWpDQkNHY2dsRmFmanR6Sm10VmZWN2dCWk5JSmlVQmFpZm1GSldRS2pfSSIsInNjb3BlIjp7ImRvY3VtZW50IjoiZXF4TUNJVFN5SmE1Qy9RaU90ZmxmQndDNVltdEZTM3A4YjdjNzJSd3ptST0ifSwiaWF0IjoxNTE2MjM5MDIyLCJzdWJfandrIjp7Imt0eSI6IlJTQSIsIm4iOiJsS3pHXzdNTlJ6aDBjSkVXaDVhbU9fMldDV1ZaeE1mMFlRTGZwZEZkZzI1Vm41RDdFRXg1UUN1WjBmT2lEdkJtWS00QTd1RWlYMXNnNkRWbE1aRE82S1QzTy0tbkcwaFJKZ1NxZjladVRUWWItSnBhdnZtRjA1dmJBalQyblU2bmw4a1Nhd2t5OF9weEYyYjhBUDNWVXNWRnQtOUZpVC10WG02LUJaMTdYMFFUbXJUZzdlWUxiZWgzQzFmZkhxYkdFLURtck4yWnJXNVN0TUdhdFNORXVGZm1Zb2h5RW4zTWc3TFRvb2ZDODRSYk9FTnNkX2hXaXBmaXI2MFdkTnBwUW84MGpDSzlHOHdmR3RSSlFZdFFvbDFZazd5Zm5MR0NmYU0tM2lkZGtXTWVQNTlSZ1JvcGhraFJ1emZDb1RRbVdkZWJLRnV2NVZ4TlU2d2FWZjQ2eFEiLCJlIjoiQVFBQiJ9fQ.OHKuI8If4eoC5ziFXUk17Do3eYy090Ejs_DPHuWIxVjUdgojpUs3OAkZiJt7uNS9HC_Xq6mTX5x9rM_7bDMNKI2wYQ9A2EfVKj9WCawgcH8cn__voWvCEvg2e0xabOhPsmrAJNemsbV8rV5oDogt5wtATapeg2hl251hPsuT1VetIDOqd5D23oDz9jBbLf8Vw6bqSmDuexAJeMMGI8iWXN39xi7vmLXpN1j4XkzDa23XFEXolBAi6lwhRw0jJX7RhH9A2wVamwhrCr3nqAKVIUUYzdSJOplO5cb0VADgX0MLrv1vC1kW1DZSlwNRvrPXlnMUDprJ0BJpgvjtojdtgA";

    final String publicKeySource =
"""
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlKzG/7MNRzh0cJEWh5am
O/2WCWVZxMf0YQLfpdFdg25Vn5D7EEx5QCuZ0fOiDvBmY+4A7uEiX1sg6DVlMZDO
6KT3O++nG0hRJgSqf9ZuTTYb+JpavvmF05vbAjT2nU6nl8kSawky8/pxF2b8AP3V
UsVFt+9FiT+tXm6+BZ17X0QTmrTg7eYLbeh3C1ffHqbGE+DmrN2ZrW5StMGatSNE
uFfmYohyEn3Mg7LToofC84RbOENsd/hWipfir60WdNppQo80jCK9G8wfGtRJQYtQ
ol1Yk7yfnLGCfaM+3iddkWMeP59RgRophkhRuzfCoTQmWdebKFuv5VxNU6waVf46
xQIDAQAB
-----END PUBLIC KEY-----""";

    final String privateKeySource =
"""
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCUrMb/sw1HOHRw
kRaHlqY7/ZYJZVnEx/RhAt+l0V2DblWfkPsQTHlAK5nR86IO8GZj7gDu4SJfWyDo
NWUxkM7opPc776cbSFEmBKp/1m5NNhv4mlq++YXTm9sCNPadTqeXyRJrCTLz+nEX
ZvwA/dVSxUW370WJP61ebr4FnXtfRBOatODt5gtt6HcLV98epsYT4Oas3ZmtblK0
wZq1I0S4V+ZiiHISfcyDstOih8LzhFs4Q2x3+FaKl+KvrRZ02mlCjzSMIr0bzB8a
1ElBi1CiXViTvJ+csYJ9oz7eJ12RYx4/n1GBGimGSFG7N8KhNCZZ15soW6/lXE1T
rBpV/jrFAgMBAAECggEAEY3c6TAA71nRFcFrwZKeChNE9dMLqFpblmC6e1+fBPew
EGkT27bL0jHTtaBokA7+Z+ogHhRi9JNT7VbwncuVQo7HykHfny8Itc5zOyjcWS18
W/wNnuLjVDrgvv/15lj1HMzW6eBqLbhyResY8h0GZQF529/ae/wSwkWdFhikCnlX
EvE3vZlTiPUbFGfrTDUabuuOCI8Zvqa8ikoHzHwhCp+7Yqac0IOzzuIelsZH+mYo
U/pQgI3XBAnBSRpwdnMR8DE4jJymiyxijjQiomslgHbtQds5RV6naEHEVKDzPBIx
MT6q8r4Z4eO72EYETQa43WQRJbaPGgn6mTIOAvtn+QKBgQDQldQyWiEFUR4N91u2
lZTwbPuvF3koMZJE7Cj8RBpLjRpVXjXIfuItpSCRMyxor18Ch+uhSBfhJafeQfLV
sYreQeRXG0rqkHVp2NkOkY/NTlMRQAkMnXkO3tuVGurOLw3+1/g3rJbQOIWzyU+F
9jP90ehO/Z+G5G/6+Lfz5krkAwKBgQC2eJgS/d27mVyjYydgC5qUm+l8hpl/yLrM
cV6FesOUcBg/fIxgsIF5VVfq7sQA/AT3fC6fOTHsoZZROFcEUrXCE/MpNR7p6DTW
6tENnAHeH0p4Q71DcdL98fqojv0LWOO8ouB2Fh2yGaIjRWoajramgliNJnGhE63F
2A3Icec/lwKBgEsVVAAYD4DhdUcjvoDYEXgGUwbxFSzRw0xKEmrUfP6rOUp3kjas
7e/uW+3IlPjOAA5iLQCueeIS8v1/kdE4irt8zpxId8w92fYTF/c1kKUfx6kS+u7u
lJA0yqg+uz0jFQ+iYHhyCQvE/OP92EFpoceBUvV0OIOPIXwfIeCLCnRPAoGAWxVz
g/dpWZ3CWmTCYPUp4oi5Ukm2Ec1XQXpN3n9bZZtW30oCoLk6yCBL1J//AybKK+e3
Ng/M1fwLFtvyw9utkwV2WdmEZupchfONCygW3pPsDoBx11rYIZ9Kqh1GPwXOGHmN
ZiTACgk1oDmkL0GR9ygVn8/S5L73/UxOBQKmll0CgYEAzInr6Ban7uYhtVcuugO8
wWyw+pXzgzF6KK/DDcUQpfeu5vN0ixzssxaBg2kxFnQFLRnOCUy/Oee4jo5yhmAT
mzUTbQb3QghHObzXXB7t/zqwNBDC4bp/Zbyka3yRtCSSyGRRj8nNmSzVqaeqapk6
6cusb2iF4fNgaHuuMCuhMLs=
-----END PRIVATE KEY-----""";

    @Test
    public void noSignatureJwt() throws Exception {
        ControlJwt testJwt = new ControlJwt(jwt);
    }

    @Test
    public void verifyClaims() throws Exception {
        final String privateKeyContent = privateKeySource.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");
        final String publicKeyContent = publicKeySource.replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");;
        KeyFactory kf = KeyFactory.getInstance("RSA");
        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
        PrivateKey privKey = kf.generatePrivate(keySpecPKCS8);
        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyContent));
        RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(keySpecX509);
        ControlJwt testJwt = new ControlJwt(jwt);
        assertEquals(testJwt.getSubjectJwk().thumbprint().toString(), testJwt.getClaims(pubKey).getSubject());
    }

}

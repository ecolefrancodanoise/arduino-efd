package darksidecommander;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import java.io.File;
import java.io.IOException;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.security.test.context.support.WithMockUser;


@ContextConfiguration(classes = { WebConfiguration.class, StorageService.class, DataSampleController.class })

@SpringBootTest(properties = { "rootdir=/tmp/darksidetest/${random.uuid}" })
@AutoConfigureMockMvc
@WithMockUser(roles={"POST"})
public class DataSampleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DataSampleController readWriteRequestController;

    @Test
    public void fileIsWrittenNegativeLookup() throws IOException {
        readWriteRequestController.writeDataSample("robot-1", "commands", new MockMultipartFile("file", "aa1".getBytes()));
        readWriteRequestController.writeDataSample("robot-1", "commands", new MockMultipartFile("file", "aa2".getBytes()));
        readWriteRequestController.writeDataSample("robot-1", "commands", new MockMultipartFile("file", "aa3".getBytes()));
        final String[] fileList = new File("robot-1/commands").list();
        byte[] data1 = readWriteRequestController.readDataSample("robot-1", -3, "commands").getBody();
        byte[] data2 = readWriteRequestController.readDataSample("robot-1", -2, "commands").getBody();
        assertEquals("aa1", new String(data1));
        assertEquals("aa2", new String(data2));
    }
}

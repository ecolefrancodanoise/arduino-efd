describe("FairCert Test", function() {
    const carrierPublicKeyFromPem = KEYUTIL.getKey(carrierPublicKey);
    const carrierPublicKeyJWK = KEYUTIL.getJWKFromKey(carrierPublicKeyFromPem);
    const shipperPublicKeyFromPem = KEYUTIL.getKey(shipperPublicKey);
    const shipperPublicKeyJWK = KEYUTIL.getJWKFromKey(shipperPublicKeyFromPem);
    const carrierPrivateKeyFromPem = KEYUTIL.getKeyFromPlainPrivatePKCS8PEM(carrierPrivateKey);
    const shipperPrivateKeyFromPem = KEYUTIL.getKeyFromPlainPrivatePKCS8PEM(shipperPrivateKey);
    const carrierURL = "";
    const shipperURL = "";

    beforeEach(async function() {
    });

    it("should have valid PKCS8 signature", function() {
        let controlJwt =  KJUR.jws.JWS.sign(null, {alg: "RS256"},
                                JSON.stringify({"iss": carrierURL, "sub": shipperURL, "sub_jwk": shipperPublicKeyJWK}),
                                carrierPrivateKeyFromPem);        
        expect(KJUR.jws.JWS.verifyJWT(controlJwt, carrierPublicKeyFromPem, {alg: ['RS256']})).toEqual(true);
    });

});

describe("FairCert POST endpoint test", function() {
    const carrierPublicKeyFromPem = KEYUTIL.getKey(carrierPublicKey);
    const carrierPublicKeyJWK = KEYUTIL.getJWKFromKey(carrierPublicKeyFromPem);
    const shipperPublicKeyFromPem = KEYUTIL.getKey(shipperPublicKey);
    const shipperPublicKeyJWK = KEYUTIL.getJWKFromKey(shipperPublicKeyFromPem);
    const carrierPrivateKeyFromPem = KEYUTIL.getKeyFromPlainPrivatePKCS8PEM(carrierPrivateKey);
    const shipperPrivateKeyFromPem = KEYUTIL.getKeyFromPlainPrivatePKCS8PEM(shipperPrivateKey);
    const resourceIdentifier = self.crypto.randomUUID();
    const carrierURL = "";
    const shipperURL = "";

    it("should be able to initially POST to /{resourceIdentifier}/control, subsequently not", async function() {
        let controlJwt =  KJUR.jws.JWS.sign(null, {alg: "RS256"},
                                JSON.stringify({"iss": carrierURL, "sub": shipperURL, "sub_jwk": shipperPublicKeyJWK}),
                                carrierPrivateKeyFromPem);        
        expect(KJUR.jws.JWS.verifyJWT(controlJwt, carrierPublicKeyFromPem, {alg: ['RS256']})).toEqual(true);
        const jwtString = JSON.stringify(controlJwt);
        await fetch('/' + resourceIdentifier + '/control', {
                        method: 'post',
                        headers: {'Access-Control-Allow-Origin': '*'},
                        body: jwtString
                    });
        const response = await fetch('/' + resourceIdentifier + '/control/0', {headers: {'Access-Control-Allow-Origin': '*'}});
        expect(await response.text()).toEqual(jwtString);
        const responseUnauthorizedPost = await fetch('/' + resourceIdentifier + '/control', {
                        method: 'post',
                        headers: {'Access-Control-Allow-Origin': '*'},
                        body: jwtString
                    });
        expect(await responseUnauthorizedPost.status).toEqual(403);
        let authorizationJwt =  KJUR.jws.JWS.sign(null, {alg: "RS256"},
                                JSON.stringify({"iat": Date.now()}),
                                shipperPrivateKeyFromPem);
        let controlJwt2 =  KJUR.jws.JWS.sign(null, {alg: "RS256"},
                                JSON.stringify({"iss": shipperURL, "sub": carrierURL, "sub_jwk": carrierPublicKeyJWK}),
                                shipperPrivateKeyFromPem);  
        const responseAuthorizedPost = await fetch('/' + resourceIdentifier + '/control', {
                        headers: {"Authorization" : "Bearer " + authorizationJwt, 'Access-Control-Allow-Origin': '*'},
                        method: 'post',
                        body: JSON.stringify(controlJwt2)
                    });
        expect(await responseAuthorizedPost.status).toEqual(200);
    });

});

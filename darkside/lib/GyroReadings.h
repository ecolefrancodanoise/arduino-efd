#ifndef GYROREADINGS_H
#define GYROREADINGS_H

#include <BasicLinearAlgebra.h>
struct GyroReadings {
    BLA::Matrix <3> yprMat;
    BLA::Matrix <3> gyro;
    BLA::Matrix <3> accelerations; //expectedly for debugging only
};

#endif

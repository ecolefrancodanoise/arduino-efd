#ifndef HTTPSTREAMCLIENT_H
#define HTTPSTREAMCLIENT_H

#include "Client.h"

#ifdef ARDUINO_ARCH_SAMD
    #include <stdarg.h>
    #define sprintf_P sprintf
    #define vsprintf_P vsprintf
#endif

#ifndef serialTimeOut
#define serialTimeOut 1000
#endif

#define TCP_BUF_SIZE 96

#define ETAGSIZE 5

/*
 * A class for making HTTP GET and POST (file upload) requests.
 */

class HttpStreamClient {
public:
    HttpStreamClient(Client* _tcpStream);
    const char* get(const char* host, const char* path, int port);
    const char* postFile(const char* host, const char* path, int port, Stream* fileContent, int contentLength);

private:
    Client* tcpStream;
    char tcpBuffer[TCP_BUF_SIZE] = {0};
    void parseHttpResponse();
    bool tcp_printf(const char* format, ...);
    char ETag[ETAGSIZE+1] = {0}; //+1 for 0-terminator
};

HttpStreamClient::HttpStreamClient(Client* _tcpStream) : tcpStream(_tcpStream) {}

bool HttpStreamClient::tcp_printf(const char* format, ...) {
    va_list arglist;
    va_start(arglist, format);
    vsprintf_P(tcpBuffer, format, arglist); //no need to memset 0, tcpBuffer is overwritten as a string
    va_end(arglist);
    return tcpStream->write((const uint8_t*)tcpBuffer, strlen(tcpBuffer));
}

const char* HttpStreamClient::get(const char* host, const char* path, int port) {
    if(!tcpStream->connect(host, port)) return 0;
    tcp_printf(PSTR("GET "));
    tcp_printf(PSTR("%s"), path);
    tcp_printf(PSTR(" HTTP/1.1\r\n"));
    if(strlen(ETag) > 0) {
        tcp_printf(PSTR("If-None-Match: \"%s\"\r\n"), ETag);
    }
    tcp_printf(PSTR("Host:"));
    tcp_printf(PSTR("%s"), host);
    bool connectionOk = tcp_printf(PSTR("\r\nConnection: close\r\n\r\n"));
    if (connectionOk) {
        parseHttpResponse();
    }
    return tcpBuffer;
}

const char* HttpStreamClient::postFile(const char* host, const char* path, int port, Stream* fileContent, int contentLength) {
    if(!tcpStream->connect(host, port)) return 0;
    const char* contentDelimiter = "----dBHkGVB97sEC";
    const int entityBodyWrapping = 2 * strlen(contentDelimiter) + 90 + 21 + 8 ; //includes misc -'ses \r\n's and such
    tcp_printf(PSTR("POST "));
    tcp_printf(PSTR("%s"), path);
    tcp_printf(PSTR(" HTTP/1.0\r\nHost: "));
    tcp_printf(PSTR("%s\r\n"), host);
    tcp_printf(PSTR("Content-Length: %d\r\n"), contentLength + entityBodyWrapping);
    tcp_printf(PSTR("Content-Type: multipart/form-data; boundary="));
    tcp_printf(PSTR("%s\r\n\r\n--%s\r\n"), contentDelimiter, contentDelimiter);
    tcp_printf(PSTR("Content-Disposition: form-data; name=\"file\"; filename=\"img.jpg\"\r\n"));
    tcp_printf(PSTR("Content-Type: image/jpg\r\nConnection: close\r\n\r\n"));
    int pos = 0;
    while (fileContent->available()) {
        const char tmp = fileContent->read();
        tcpBuffer[pos] = tmp;
        if (pos == TCP_BUF_SIZE - 1) {
            tcpStream->write((const uint8_t*)tcpBuffer, TCP_BUF_SIZE);
            pos = 0;
            memset(tcpBuffer, 0, sizeof tcpBuffer);
        } else {
            ++pos;
        }
    }
    if (pos > 0 && pos < TCP_BUF_SIZE) {
        tcpStream->write((const uint8_t*)tcpBuffer, pos);
    }
    sprintf(tcpBuffer, "\r\n--%s--\r\n", contentDelimiter);
    bool connectionOk = tcpStream->write((const uint8_t*)tcpBuffer, strlen(tcpBuffer));
    if (connectionOk) {
        parseHttpResponse();
    }
    return tcpBuffer;
}

void HttpStreamClient::parseHttpResponse() {
    long int timer = millis();
    while(!tcpStream->available() && (millis() - timer < serialTimeOut)) {
        //busy wait
    };
    int i = 0;
    bool headerParsed = false;
    while(tcpStream->available() && !headerParsed) {
        char tmp = tcpStream->read();
        tcpBuffer[i++] = tmp;
        if (tmp == '\n') {
            const char* ETagPosition = strstr(tcpBuffer, "ETag: ");
            if (ETagPosition) {
                const char* ETagStartPos = ETagPosition + 7;
                memcpy(ETag, ETagStartPos, ETAGSIZE);
            }
            if (i <= 2) { //line consisting of "\r\n" reached
                headerParsed = true;
            }
            memset(tcpBuffer, 0, sizeof tcpBuffer);
            i = 0;
        }
    }
    memset(tcpBuffer, 0, sizeof tcpBuffer);
    i = 0;
    while(tcpStream->available() && i < TCP_BUF_SIZE) {
        char tmp = tcpStream->read();
        tcpBuffer[i++] = tmp;
    }
    tcpStream->flush();
}

#endif

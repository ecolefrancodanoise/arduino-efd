#ifndef SIM7000CLIENT_H
#define SIM7000CLIENT_H

#include "AbstractTcpClient.h"

/*
 * A class for managing the communication through the AT command interface
 * of the SIMCom SIM7000 (GSM + GPS device)
 *
 * The AT test sequence originally used to get the unit working:
 * AT+COPS=?
 * AT+CREG?
 * AT+CGREG?
 * AT+CGACT?
 * AT+CGDCONT=2,"IP","data.tre.dk","0.0.0.0",0,0,0,0
 * AT+CGACT=1,2
 * AT+CGACT?
 * AT+CDNSCFG?
 * AT+CIPCSGP?
 * AT+CIPSHUT
 * AT+CSTT="data.tre.dk","",""
 * AT+CIICR
 * AT+CIFSR
 * AT+CIPHEAD=1
 * AT+CDNSCFG?
 * AT+CDNSGIP="darkside.ecolefrancodanoise.dk"
 * AT+CIPSTART="TCP","darkside.ecolefrancodanoise.dk",80
 * AT+CIPSEND=78
GET /test HTTP/1.0
Host:darkside.ecolefrancodanoise.dk
Connection: close
 */

/*
 * A struct containing a GPS position (and time).
 * A time of 0 indicates that the whole sample is invalid.
 */
const int TimeDescriptorSize = 15;
struct LatLonTime {
    long int lat;
    long int lon;
    char time[TimeDescriptorSize] = "";
};

class Sim7000Client : public AbstractTcpClient {
  public:
    Sim7000Client(Stream* ss);
    virtual bool begin(const char* pin, const char* dummy = 0x0);
    virtual int connect(const char* host, uint16_t port);
    const char* response(void) {return lastResponse;}
    LatLonTime gpsLatLonTime();
};

Sim7000Client::Sim7000Client(Stream* ss) : AbstractTcpClient(ss) {}

bool Sim7000Client::begin(const char* pin, const char* dummy) {
    ss->flush(); //flushing here, as we cannot know what might be in the buffer already
    if (pin != 0x0) {
        ss->print(F("AT+CPIN="));
        ss->println(pin);
        checkSerialResponseFor("OK\r\n", errorTerminator, serialTimeOut); //error 16 if pin already entered
    }
    ss->println(F("AT+CGREG?"));
    if(!checkSerialResponseFor("+CGREG: 0,1\r\n\r\nOK\r\n", errorTerminator, serialTimeOut)) { //not connected on home network
        ss->println(F("AT+CREG?"));
        if(!checkSerialResponseFor("+CREG: 0,5\r\n\r\n", errorTerminator, serialTimeOut)) return false;
        ss->print(F("AT+CGDCONT=2,\"IP\",\""));
        ss->print(GPRS_APN);
        ss->println(F("\",\"0.0.0.0\",0,0,0,0"));
        if (!checkSerialResponseFor("OK\r\n", errorTerminator, serialTimeOut)) return false;
        ss->println(F("AT+CGACT=1,2"));
        if (!checkSerialResponseFor("OK\r\n", errorTerminator, serialTimeOut)) return false;
    }
    ss->println(F("AT+CIPSHUT"));
    if (!checkSerialResponseFor("OK\r\n", errorTerminator, serialTimeOut)) return false;
    ss->print(F("AT+CSTT=\""));
    ss->print(GPRS_APN);
    ss->println(F("\",\"\",\"\""));
    if (!checkSerialResponseFor("OK\r\n", errorTerminator, serialTimeOut)) return false;
    ss->println(F("AT+CIICR"));
    if (!checkSerialResponseFor("OK\r\n", errorTerminator, 20*serialTimeOut)) return false;
    ss->println(F("AT+CIFSR")); //gets the local IP. Seems to be needed for some reason
    if (!checkSerialResponseFor("\r\n", errorTerminator, serialTimeOut)) return false;
    ss->println(F("AT+CIPHEAD=1")); //turn +IPR syntax when receiving data
    if (!checkSerialResponseFor("\r\n", errorTerminator, serialTimeOut)) return false;
    ss->println("AT+CIPSPRT=1"); // ensure '>' prompt after SEND
    checkSerialResponseFor("OK\r\n", errorTerminator, serialTimeOut);
    return true;
}

int Sim7000Client::connect(const char* host, uint16_t port) {
    ss->flush();
    char sendBuffer[TCP_BUF_SIZE];
    sprintf_P(sendBuffer, PSTR("AT+CIPSTART=\"TCP\",\"%s\",%d"), host, port);
    ss->println(sendBuffer);
    if (!checkSerialResponseFor("CONNECT OK", errorTerminator, 10*serialTimeOut)) return 0;
    return true;
}

LatLonTime Sim7000Client::gpsLatLonTime() {
    ss->flush(); //flushing here, as we cannot know what might be in the buffer already
    ss->println("AT+CGNSPWR?");
    if (!checkSerialResponseFor("+CGNSPWR: 1\r\n\r\nOK\r\n", "+CGNSPWR: 0\r\n\r\nOK\r\n", serialTimeOut)) {
        ss->println("AT+CGNSPWR=1"); //turn on GPS
        checkSerialResponseFor("OK\r\n", errorTerminator, serialTimeOut);
    };
    LatLonTime gpsSample;
    memset(gpsSample.time, 0, TimeDescriptorSize);
    ss->println(F("AT+CGNSINF"));
    //Response format: +CGNSINF: 1,1,20190614171311.000,55.680975,12.548965,
    if (!checkSerialResponseFor("+CGNSINF: 1,1,", errorTerminator, serialTimeOut)) {
        return gpsSample;
    }
    checkSerialResponseFor(",","\n",serialTimeOut);
    memcpy(gpsSample.time, lastResponse, 14);
    checkSerialResponseFor(",","\n",serialTimeOut);
    gpsSample.lat = atof(lastResponse) * 1000000;
    checkSerialResponseFor(",","\n",serialTimeOut);
    gpsSample.lon = atof(lastResponse) * 1000000;
    checkSerialResponseFor("OK\r\n", errorTerminator, serialTimeOut);
    return gpsSample;
}

#endif

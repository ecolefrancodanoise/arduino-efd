#ifndef TESTSTREAM_H
#define TESTSTREAM_H

class TestStream : public Stream {
public:
    int available() {return strlen(testString) - pos;};
    int read() {return (int)testString[pos++];};
    size_t write(uint8_t) {};
    int peek() {};
private:
    const char* testString = "this is a test string";
    int pos = 0;
};

#endif

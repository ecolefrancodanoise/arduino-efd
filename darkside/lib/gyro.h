/*
 * MPU6050 gyroscope providing both angle and angular velocity
 * Complementary filtering is done in the MPU6050
 */

#include "MPU6050_6Axis_MotionApps20.h"
#include "BasicLinearAlgebra.h"
#include "GyroReadings.h"

using BLA::Matrix;

Matrix <3> operator*(float c, Matrix <3> M) {
    return Matrix <3> ({c*M(0), c*M(1), c*M(2)});
}

struct Gyro {

    void init() {
        Wire.begin();
        mpu.initialize();
        mpu.dmpInitialize();
        mpu.CalibrateAccel(6);
        mpu.CalibrateGyro(6);
        mpu.setDMPEnabled(true);
    }

    bool isReady() {
        return mpu.dmpGetCurrentFIFOPacket(fifoBuffer);
    }

    GyroReadings read() {
        Quaternion q;           // [w, x, y, z]         quaternion container
        VectorFloat gravity;    // [x, y, z]            gravity vector
        float ypr[3];           // [yaw, pitch, roll]   rotate clockwise, pitch nose up, roll rightwards
        VectorInt16 gy;         // [roll, pitch, yaw]   roll rightwards, pitch nose down, rotate CCW
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetGravity(&gravity, &q);
        mpu.dmpGetYawPitchRoll(ypr, &q, &gravity);
        mpu.dmpGetGyro(&gy, fifoBuffer);
        const Matrix <3> yprMat = 0.2 * (const BLA::Matrix <3>) {0, ypr[1], ypr[2]} + 0.8 * previousYpr; //yaw should be free and not be penalized
        const Matrix <3> gyro = {-gy.z, -gy.y, gy.x};
        gyro = 0.4 * gyro + 0.6 * previousGyro; //low-pass filter the derivative
        previousGyro = gyro;
        previousYpr = yprMat;
        return GyroReadings({yprMat, gyro, {gravity.x, gravity.y, gravity.z}});
    }

    private:
        MPU6050 mpu;
        uint8_t fifoBuffer[64];
        Matrix<3> previousGyro = {0.0, 0.0, 0.0};
        Matrix<3> previousYpr = {0.0, 0.0, 0.0};
        Matrix <3> gyro;

};

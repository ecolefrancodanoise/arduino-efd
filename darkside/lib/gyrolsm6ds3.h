/*
 * LSM6DS3 gyroscope providing acceleration and angular velocity
 */

#include <Arduino_LSM6DS3.h>
#include "BasicLinearAlgebra.h"
#include "GyroReadings.h"
#define SAMPLE_RATE 100

using BLA::Matrix;

Matrix <3> operator*(float c, Matrix <3> M) {
    return Matrix <3> ({c*M(0), c*M(1), c*M(2)});
}

struct GyroLSM6DS3   {

    void init() {
        IMU.begin();
        const float nbCalibrationSamples = 200;
        for (int i = 0; i < nbCalibrationSamples; ++i) {
            while(!isReady()) {
                //wait for imu to have data
            }
            float gx, gy, gz;
            IMU.readGyroscope(gx, gy, gz); // -pitch' , -roll', -yaw'
            gyroOffset += Matrix <3> ({-gz, -gx, -gy});
        }
        gyroOffset /= nbCalibrationSamples;
    }

    bool isReady() {
        return IMU.accelerationAvailable() && IMU.gyroscopeAvailable();
    }

    GyroReadings read() {
        GyroReadings readings = readRawIMUValues();
        readings.gyro -= gyroOffset;
        // gyro = 0.4*readings.gyro + 0.6 * gyro; //low pass filter angular velocity
        gyro = 0.9*readings.gyro + 0.1 * gyro; //low pass filter angular velocity
        const float sampleTime = 1.0f/100;
        yprMat += gyro * sampleTime;
        yprMat(0) = 0.0;// ignore yaw (set to 0)
        yprMat = 0.95 * yprMat + 0.05 * readings.yprMat; //complementary filter
        return GyroReadings({yprMat, gyro, readings.accelerations});
    }

    private:
        Matrix <3> gyro = {0.0, 0.0, 0.0};
        Matrix <3> yprMat = {0.0, 0.0, 0.0};
        Matrix <3> gyroOffset = {0.0, 0.0, 0.0};
        Matrix <3> yprFromGravity(Matrix<3> acceleration) {
            return { 0.0,                             // ignore yaw (set to 0)
                    -atan2(acceleration(0), acceleration(2))*RAD_TO_DEG, atan2(acceleration(1),
                    acceleration(2))*RAD_TO_DEG};
        }
        GyroReadings readRawIMUValues() {
            Matrix <3> acceleration;
            Matrix <3> gyroReading;
            IMU.readAcceleration(acceleration(1), acceleration(0), acceleration(2)); //mapping (x, y, z) to (fwd, right, down)
            IMU.readGyroscope(gyroReading(1), gyroReading(2), gyroReading(0)); //mapping (gy1, gy2, gy3) to (yaw CCW, pitch nose up, roll right)
            gyroReading = - gyroReading;
            return GyroReadings({yprFromGravity(acceleration), gyroReading, acceleration});
        }
};

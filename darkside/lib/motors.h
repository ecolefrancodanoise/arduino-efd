/*
 * Motor configuration
 * forward
 *  0(CW)  1(CCW)
 *  3(CCW) 2(CW)
 * back
 */
#include <Servo.h>

struct Motors {
    Motors(const int motorPins_[4]) : motorPins(motorPins_) {}

    void init() {
        for (int i = 0; i < 4; ++i) {
            motors[i].attach(motorPins[i]);
            motors[i].writeMicroseconds(escBasePulseWidth);    
        }
    }

    void activate(const Matrix <4> motorPowers, float throttle) {
        for (int i = 0; i < 4; ++i) {
            motors[i].writeMicroseconds(motorPowers(i) + throttle + escBasePulseWidth);
        }
    }

    private:
        int* motorPins;
        Servo motors[4];
        const int escBasePulseWidth = 1000;

};

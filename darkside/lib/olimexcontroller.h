#ifndef olimexcontroller
#define olimexcontroller

/**
 * Controller for the Olimex BB-L298
 * https://www.olimex.com/Products/Robot-CNC-Parts/MotorDrivers/BB-L298/open-source-hardware
 * Pin layout:
 * 1. 5V VCC
 * 2. Enable A (PWM)
 * 3. IN1
 * 4. IN2
 * 5. Enable B (PWM)
 * 6. IN3
 * 7. IN4
 * 8. GND
 */

struct OlimexController {
    const int directionLeftPin1; //IN1
    const int directionLeftPin2; //IN2
    const int directionRightPin1; //IN3
    const int directionRightPin2; //IN4
    const int powerPinLeft; //Enable A
    const int powerPinRight; //Enable B
    const float leftMotorCalibration;
    const float rightMotorCalibration;
    const bool usePowerPins;

    /*
     * Constructor without power pins (for instance when setting Enable pins to VCC)
     */
    OlimexController(int _directionLeftPin1, int _directionLeftPin2, int _directionRightPin1, int _directionRightPin2) :
        directionLeftPin1(_directionLeftPin1), directionLeftPin2(_directionLeftPin2),
        directionRightPin1(_directionRightPin1), directionRightPin2(_directionRightPin2),
        powerPinLeft(-1), powerPinRight(-1), leftMotorCalibration(0.0), rightMotorCalibration(0.0), usePowerPins(false) {}

    /*
     * Constructor with power pins and calibration (have to be PWM capable)
     */
    OlimexController(int _powerPinLeft, int _directionLeftPin1, int _directionLeftPin2, int _powerPinRight, int _directionRightPin1, int _directionRightPin2, float _leftMotorCalibration = 1.0, float _rightMotorCalibration = 1.0) :
        directionLeftPin1(_directionLeftPin1), directionLeftPin2(_directionLeftPin2),
        directionRightPin1(_directionRightPin1), directionRightPin2(_directionRightPin2),
        powerPinLeft(_powerPinLeft), powerPinRight(_powerPinRight), usePowerPins(true),
        leftMotorCalibration(_leftMotorCalibration), rightMotorCalibration(_rightMotorCalibration) {}

    void setup() {
        pinMode(directionLeftPin1, OUTPUT);
        pinMode(directionLeftPin2, OUTPUT);
        pinMode(directionRightPin1, OUTPUT);
        pinMode(directionRightPin2, OUTPUT);
        if (usePowerPins) {
            pinMode(powerPinLeft, OUTPUT);
            pinMode(powerPinRight, OUTPUT);
        }

        digitalWrite(directionLeftPin1, LOW);
        digitalWrite(directionLeftPin2, LOW);
        digitalWrite(directionRightPin1, LOW);
        digitalWrite(directionRightPin2, LOW);
        if (usePowerPins) {
            analogWrite(powerPinLeft, 0);
            analogWrite(powerPinRight, 0);
        }
    }

    void execute (char command, int power = 0) {
        switch(command) {
            case 'w': //forward
                digitalWrite(directionLeftPin1, HIGH);
                digitalWrite(directionLeftPin2, LOW);
                digitalWrite(directionRightPin1, HIGH);
                digitalWrite(directionRightPin2, LOW);
                break;
            case 's': //back
                digitalWrite(directionLeftPin1, LOW);
                digitalWrite(directionLeftPin2, HIGH);
                digitalWrite(directionRightPin1, LOW);
                digitalWrite(directionRightPin2, HIGH);
                break;
            case 'a': //left
                digitalWrite(directionLeftPin1, LOW);
                digitalWrite(directionLeftPin2, HIGH);
                digitalWrite(directionRightPin1, HIGH);
                digitalWrite(directionRightPin2, LOW);
                break;
            case 'd': //right
                digitalWrite(directionLeftPin1, HIGH);
                digitalWrite(directionLeftPin2, LOW);
                digitalWrite(directionRightPin1, LOW);
                digitalWrite(directionRightPin2, HIGH);
                break;
            case 'x': //stop
            default:
                digitalWrite(directionLeftPin1, LOW);
                digitalWrite(directionLeftPin2, LOW);
                digitalWrite(directionRightPin1, LOW);
                digitalWrite(directionRightPin2, LOW);
                break;
        }
        if (usePowerPins) {
            analogWrite(powerPinLeft, leftMotorCalibration * power);
            analogWrite(powerPinRight, rightMotorCalibration * power);
        }
    }

    //motorId should be 0 (left) or 1 (right)
    //motorPower should be between -255 (full reverse) and 255 (full forward)
    void setPower(int motorId, int motorPower) {
        digitalWrite(motorId == 0? directionLeftPin1 : directionRightPin1, motorPower > 0? HIGH : LOW);
        digitalWrite(motorId == 0? directionLeftPin2 : directionRightPin2, motorPower > 0? LOW : HIGH);
        analogWrite(motorId == 0? powerPinLeft : powerPinRight, motorPower > 0 ? motorPower : -motorPower);
    }
};

#endif

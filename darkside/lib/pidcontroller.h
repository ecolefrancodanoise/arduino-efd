/*
 * A PD filter transforming the measured error (and its derivative) into motor powers
 * The derivative (angular velocity) is provided by the MPU6050 hardware and (noise) filtering
 * is done in the Gyro struct.
 */
#include "BasicLinearAlgebra.h"

using BLA::Matrix;

struct PidController {
    const float Kp;
    const float Kd;
    const Matrix <3> yprMat;
    const Matrix <3> gyro;
    const Matrix <3> target;
    Matrix <4> motorPowers() const {
        const Matrix <4, 3> errorToAction = {  1, -1, -1,
                                              -1, -1,  1,
                                               1,  1,  1,
                                              -1,  1, -1 };
        return errorToAction * (Kp * yprMat + Kd * gyro - target);
    }
};

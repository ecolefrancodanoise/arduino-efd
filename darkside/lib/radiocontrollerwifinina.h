/*
 * WiFi-based radio controller
 *
 * Set up the joystick to send via netcat (extract the channel and value positions, then send):
 * old:
 *  cat  /dev/input/event20 | stdbuf -o0 xxd -c 24 -ps | stdbuf -o0 cut -c33,34,37,38,41,42 | stdbuf -o0 grep -v "^00" | stdbuf -o0 xxd -r -p | nc 192.168.4.1 3000
 * new:
stty -icanon
xset r rate 160 30
nc 192.168.4.1 3000
bbe -e 's/\xAB/erase/' tmp |grep -av erase > cleanseddata
 */

#include <WiFiNINA.h>
#include <BasicLinearAlgebra.h>
#include "GyroReadings.h"
#define NUMBER_OF_CHANNELS 4

struct RadioControllerWifiNina {
    const char* ssid;
    bool isArmed = false;

    RadioControllerWifiNina(const char* _ssid) : ssid(_ssid), tcpServer(WiFiServer(3000)), values({0, 0, 0, 0}) {};

    float yaw() {
        return values[0] + yawTrim;
    }

    float pitch() {
        return values[1];
    }

    float roll() {
        return values[2];
    }

    float throttle() {
        return values[3];
    }

    void init() {
        WiFi.beginAP(ssid);
        tcpServer.begin();
        values[0] = 0;
        values[1] = 0;
        values[2] = 0;
        values[3] = -127;
    }

    int read() {
        values[0] *= 0.99;
        values[1] *= 0.99;
        values[2] *= 0.99;
        if (WiFiClient newClient = tcpServer.available()) {
            client = newClient;
        }
        if (client && client.available()) {
            uint8_t tcpRxBuffer[296];
            const int nbBytesRead = client.read(tcpRxBuffer, 296);
            client.write(tcpRxBuffer, strlen(tcpRxBuffer));
            for (int i = 0; i < nbBytesRead; i++) {
                switch(tcpRxBuffer[i]) {
                    case 'a': values[0] -= 60; break;
                    case 'w': values[3] += 10; break;
                    case 's': values[3] -= 10; break;
                    case 'd': values[0] += 60; break;
                    case 'i': values[1] -= 60; break;
                    case 'j': values[2] -= 60; break;
                    case 'k': values[1] += 60; break;
                    case 'l': values[2] += 60; break;
                    case 'Q': isArmed = !isArmed; break;
                    case 'q': yawTrim -= 10; break;
                    case 'e': yawTrim += 10; break;
                }
            }
            for (int i  = 0;  i < NUMBER_OF_CHANNELS; ++i) {
                values[i] =  constrain(values[i], -127,  128);
            }
            return nbBytesRead;
        }
        return 0;
    }

    void write(const GyroReadings& gyroReadings, const BLA::Matrix <4>& motorPowers, float throttle, float rcValues[NUMBER_OF_CHANNELS]) {
        if (client.connected()) {
            const String debugString = String() + millis() + " " + gyroReadings.gyro(0) + " " + gyroReadings.gyro(1) + " " + gyroReadings.gyro(2) + "    "
                                     + gyroReadings.yprMat(0) + " " + gyroReadings.yprMat(1) + " " + gyroReadings.yprMat(2) + "    "
                                     + gyroReadings.accelerations(0) + " " + gyroReadings.accelerations(1) + " " + gyroReadings.accelerations(2) + "    "
                                     + motorPowers(0) + " " + motorPowers(1) + " " + motorPowers(2) + " " + motorPowers(3) + "    " + throttle + "    "
                                     + rcValues[0] + " " + rcValues[1] + " " + rcValues[2] + " " + rcValues[3]
                                     + "\n";
            char* debugc_str = debugString.c_str();
            client.print(debugString);
        }
    }
    private:
        WiFiServer tcpServer;
        WiFiClient client;
        float values[NUMBER_OF_CHANNELS];
        float yawTrim = 0;
};

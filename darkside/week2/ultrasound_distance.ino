
//Ultrasonic sensor pins
int inputPin  = 4;    // define pin for sensor echo
int outputPin = 5;    // define pin for sensor trig

void setup() {
    Serial.begin(9600);
    pinMode(inputPin, INPUT);
    pinMode(outputPin, OUTPUT);
}

/*
 * Read the distance read by the ultrasound sensor
 */
void readDistance() {
    digitalWrite(outputPin, LOW);   // ultrasonic sensor transmit low level signal 2μs
    delayMicroseconds(2);
    digitalWrite(outputPin, HIGH);  // ultrasonic sensor transmit high level signal10μs, at least 10μs
    delayMicroseconds(10);
    digitalWrite(outputPin, LOW);    // keep transmitting low level signal
    float pingTime = pulseIn(inputPin, HIGH);  // read the time in between
    float distance = pingTime*0.034/2;
    Serial.println(distance);
}

void loop() {
    readDistance();
    delay(1000);
}


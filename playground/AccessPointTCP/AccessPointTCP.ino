/*
 * Sketch illustrating how to set up an access point + DHCP server.
 * To send and receive data, on host machine, run
 *   nc 192.168.4.1 3000
 */

#include <Arduino_LSM6DS3.h>
#if defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
    #include <WiFiNINA.h>
    WiFiServer tcpServer(3000);
#endif
const char ssid[] = "efdrobot";
const char password[] = "";
long int timer = 0;

void setup() {
    WiFi.beginAP(ssid);
    tcpServer.begin();
}

void loop() {
    WiFiClient client = tcpServer.available();
    char debugString[70];
    if (client == true) {
        sprintf(debugString, "Received: %5d\n", client.read());
        tcpServer.write(debugString, strlen(debugString));
    }
    if(millis() - timer > 1000) {
        timer = millis();
        sprintf(debugString, "%5d\n", analogRead(A0));
        tcpServer.write(debugString, strlen(debugString));
    }
}

#include <ArduinoBLE.h>

BLEService nanoService("13012F00-F8C3-4F4A-A8F4-15CD926DA146"); // BLE Service
BLEByteCharacteristic uuidFromCentral("13012F01-F8C3-4F4A-A8F4-15CD926DA146", BLERead | BLEWrite);

void setup() {
    Serial.begin(9600);
    BLE.begin();
    BLE.setLocalName("Arduino Nano 33 BLE Sense");
    BLE.setAdvertisedService(nanoService);

    nanoService.addCharacteristic(uuidFromCentral);

    BLE.addService(nanoService);

    BLE.advertise();
    delay(100);
    Serial.println("ProtoStax Arduino Nano BLE LED Peripheral Service Started");
}

void loop() {
    // listen for BLE centrals to connect:
    BLEDevice central = BLE.central();

    if (central) {
        while (central.connected()) {
            if (uuidFromCentral.written()) {
                    Serial.println(F("Modtaget"));
            }
        }
    }
}

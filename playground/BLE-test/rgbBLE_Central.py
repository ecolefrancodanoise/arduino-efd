import logging
import asyncio
import platform
import ast

from bleak import BleakClient
from bleak import BleakScanner
from bleak import discover

UUID_FROM_CENTRAL = '13012F01-F8C3-4F4A-A8F4-15CD926DA146'

async def setColor(client):
    val = input('Enter r to send signal to Arduino BLE:')

    if ('r' in val):
        await client.write_gatt_char(UUID_FROM_CENTRAL, True)
        print(val)

async def run():
    print('ProtoStax Arduino Nano BLE LED Peripheral Central Service')
    print('Looking for Arduino Nano 33 BLE Sense Peripheral Device...')

    found = False
    devices = await discover()
    for d in devices:
        if 'Arduino Nano 33 BLE Sense'in d.name:
            print('Found Arduino Nano 33 BLE Sense Peripheral')
            found = True
            async with BleakClient(d.address) as client:
                val = await client.read_gatt_char(UUID_FROM_CENTRAL)
                while True:
                    await setColor(client)

    if not found:
        print('Could not find Arduino Nano 33 BLE Sense Peripheral')

loop = asyncio.get_event_loop()
loop.run_until_complete(run())

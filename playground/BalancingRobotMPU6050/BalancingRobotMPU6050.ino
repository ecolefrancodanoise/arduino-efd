/*
 * A remote controlled balancing robot using an accelerometer and gyroscope (Arduino Uno Wifi Rev2).
 * The inclination of the vehicle is measured by integrating the gyroscope angular velocities. This
 * however suffers from drifting, which can be solved by averaging with a little bit of accelerometer
 * values, which are stable on the long term.
 *
 * The robot uses a tradtional PID where the error is the inclination (deviation from target angle).
 *
 * Remote control is done via netcat. To control the robot with the joystick run
 *
 *  cat  /dev/input/event20 | stdbuf -o0 xxd -c 24 -ps | stdbuf -o0 cut -c33,34,37,38,41,42 | stdbuf -o0 grep -v "^00" | stdbuf -o0 xxd -r -p | nc 192.168.4.1 3000
 *
 * where 192.168.4.1 is the IP address of the robot
 *
 */

#include "WiFiEspAT.h"
#include "MPU6050_light.h"
#include "olimexcontroller.h"

#define alpha 0.98 //weight of gyro measurement relative to accelerometer measurement (complementary filter)

int motorPower;
int prevMotorPowers[2]; //history of previously applied motor powers
float angleY;
int targetAngle = 0;
int sampleCounter = 0;
OlimexController olimexController(3, 4, 5, 6, 7, 8);
int8_t ctrlValues[] = {0, 0, 0, 0}; //target pitch, roll, yaw, power
WiFiServer tcpServer(3000);
MPU6050 mpu(Wire);
const float samplingPeriod = 1.0/100.0;//in s
long int sampleTimer = 0;
WiFiClient client;
const char ssid[] = "efdbalancer";

void setup() {
    Serial.begin(115200);
    WiFi.init(Serial);
    WiFi.beginAP(ssid);
    tcpServer.begin();
    Wire.begin();
    mpu.begin();
    mpu.calcOffsets(true,true);
}

void loop() {
    if (millis() - sampleTimer > samplingPeriod * 1000) { //in milliseconds
        if (WiFiClient newClient = tcpServer.accept()) {
            client = newClient;
        }
        uint8_t tcpRxBuffer[296];
        const int nbBytesRead = client.read(tcpRxBuffer, 296);
        for (int i = 0; i < nbBytesRead; i += 3) {
            ctrlValues[tcpRxBuffer[i+1]] = tcpRxBuffer[i+2];
        }
        const float dt = (float)(millis()-sampleTimer)/1000.0;
        sampleTimer = millis();
        mpu.update();
        const float gyroYdelta = mpu.getGyroY() * dt;
        angleY = mpu.getAngleY();
        const float estimatedVNext = 0.4*gyroYdelta - 0.01 * prevMotorPowers[0] + 20.0*sin(angleY/180*3.14);
        const float estimatedPNext = angleY + estimatedVNext;

        const float targetOrientation = 0;
        const float targetVelocity = targetOrientation - estimatedPNext;//divide by dt?
        motorPower = (targetVelocity - 0.4*estimatedVNext - 20* sin(estimatedPNext/180*3.14)) / (-0.03);

        for (int i = 0; i < 2; ++i) {
            prevMotorPowers[i] = prevMotorPowers[i+1];
        }
        prevMotorPowers[1] = motorPower;

        char debugString[70];
        sprintf(debugString, "%5d %5d %5d  %5d %5d %5d %5d  %5d\n", sampleCounter, ctrlValues[0], ctrlValues[1],
                (int)(gyroYdelta*100), (int)(angleY*100), (int)(estimatedVNext*100),(int)(estimatedPNext*100),
                motorPower);
        client.write(debugString, strlen(debugString));
    }

    if(abs(angleY) > 30) { //flip over and stop if inclination gets too large
        olimexController.setPower(0, 0);
        olimexController.setPower(1, 0);
    } else {
        olimexController.setPower(0, motorPower + 1*ctrlValues[0]);
        olimexController.setPower(1, motorPower - 1*ctrlValues[0]);
    }
}

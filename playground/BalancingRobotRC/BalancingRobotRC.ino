/*
 * A remote controlled balancing robot using an accelerometer and gyroscope (Arduino Uno Wifi Rev2).
 * The inclination of the vehicle is measured by integrating the gyroscope angular velocities. This
 * however suffers from drifting, which can be solved by averaging with a little bit of accelerometer
 * values, which are stable on the long term.
 *
 * The robot uses a tradtional PID where the error is the inclination (deviation from target angle).
 *
 * Remote control is done via netcat. To control the robot with the joystick run
 *
 *   cat  /dev/input/event20 | nc -u 192.168.43.206 3000
 *
 * where 192.168.0.41 is the IP address of the robot
 * To get the feedback us netcat to listen:
 *
 *   nc -l -u -p 9999
 *
 */

#include <Arduino_LSM6DS3.h>
#include <WiFiNINA.h>
#include <WiFiUdp.h>
#include "olimexcontroller.h"

#define Kp 30 // the proportionality coefficient between the error (in degrees) and the motor power (0-1024). Working values: 30-60. Starts shaking at 75 (@Kd = 0, 75 is ok at @Kd = 20)
#define Kd 15 // working values : 0 - 100,sometimes starts shaking at 100 (@Kp = 50)
#define sampleTime  1/104
#define alpha 0.98 //weight of gyro measurement relative to accelerometer measurement (complementary filter)
#define frequencyDivider 5 //reduce the sampling and controlling rate
#define Kprev 0.5
#define Ki 2
#define motorThreshold 20 //the minimal power need for the motors to start moving

float  gyroAngleDelta=0, currentAngle=0, prevAngle=0, error=0;
float accX, accY, accZ; //accelerations in g
float gyroX, gyroY, gyroZ; //rotational velocities in degrees per second
int motorPower;
int prevMotorPowers[4]; //history of previously applied motor powers
float currentAngleGyro, currentAngleAcc;
int targetAngle = 0;
int sampleCounter = 0;
OlimexController olimexController(3, 4, 5, 6, 7, 8);
int8_t ctrlValues[] = {0, 0, 0, 0}; //target pitch, roll, yaw, power
float errorSum = 0;

const char ssid[] = "WiFimodem-A7B6";
const char password[] = "qjy4egn4kd";
const IPAddress serverIp(192,168,0,37);
const int serverPortNumber = 9999;
WiFiUDP udp;

void setup() {
    olimexController.setup();
    IMU.begin();
    WiFi.begin(ssid, password);
    udp.begin(3000);
}

void loop() {
    if(WiFi.status() != WL_CONNECTED) {
        if(WiFi.begin(ssid, password) == WL_CONNECTED) {
            udp.begin(3000);
        }
    }
    if(udp.parsePacket() > 0) {
        uint8_t udpRxBuffer[48]; //HID packets appear to be 48 bytes long.
        udp.read(udpRxBuffer, 48);
        ctrlValues[udpRxBuffer[18]] = udpRxBuffer[16]? (int8_t)udpRxBuffer[20] : ctrlValues[udpRxBuffer[18]];
        ctrlValues[udpRxBuffer[42]] = udpRxBuffer[40]? (int8_t)udpRxBuffer[44] : ctrlValues[udpRxBuffer[42]];
    }
    if (IMU.accelerationAvailable() && IMU.gyroscopeAvailable()) {
        sampleCounter ++;
        IMU.readAcceleration(accX, accY, accZ);
        IMU.readGyroscope(gyroX, gyroY, gyroZ);
        if (sampleCounter  % frequencyDivider == 0)  {
            IMU.readAcceleration(accX, accY, accZ);
            IMU.readGyroscope(gyroX, gyroY, gyroZ);   
            gyroAngleDelta = gyroX * sampleTime * frequencyDivider;
            currentAngleGyro = (prevAngle + gyroAngleDelta);
            currentAngleAcc = atan2(accY, accZ) * RAD_TO_DEG;
            currentAngle = alpha * currentAngleGyro + (1 - alpha) * currentAngleAcc;
            error = currentAngle - 0.2*ctrlValues[0];
            errorSum += error;
            motorPower = Kp * error + Kd * gyroAngleDelta + Kprev * prevMotorPowers[0] + Ki*errorSum;
            prevAngle = currentAngle;
            motorPower += motorPower > 0? motorThreshold : -motorThreshold;
            for (int i = 0; i < 3; ++i) {
                prevMotorPowers[i] = prevMotorPowers[i+1];
            }
            prevMotorPowers[3] = motorPower;

            char debugString[70];
            sprintf(debugString, "%5d %5d %5d %5d %5d %5d %5d %5d\n", sampleCounter, ctrlValues[0], ctrlValues[2], (int)(error*100), (int)(gyroAngleDelta*100), (int)(errorSum*100),prevMotorPowers[0], motorPower);
            udp.beginPacket(serverIp, serverPortNumber);
            udp.write(debugString, strlen(debugString));
            udp.endPacket();
        }
    }

    if(abs(currentAngle) > 30) { //flip over and stop if inclination gets too large
        olimexController.setPower(0, 0);
        olimexController.setPower(1, 0);
    } else {
        olimexController.setPower(0, motorPower + 1*ctrlValues[2]);
        olimexController.setPower(1, motorPower - 1*ctrlValues[2]);
    }
}

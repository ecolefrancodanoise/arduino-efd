#include <TinyDHT.h>
#include "HttpStreamClient.h"
#include "wificonfig.h"
#include "StringStream.h"
#include "WiFiEspAT.h"
WiFiClient tcpClient;
DHT dht(7, DHT22);

HttpStreamClient httpClient(&tcpClient);
bool connectionOK = false;

long int statusTimer;
int connectionAttempts = 0;

const char* reportDiagnostic(int gas, float hum, float temp) {
    StringStream diagnosticStream;
    char diagnostic[100];
    sprintf_P(diagnostic, PSTR("gas %d<br>hum: %d<br>tempu: %d"), gas, (int)hum, (int)temp);
    diagnosticStream.setString(diagnostic);
    char diagnosticRequest[50];
    sprintf_P(diagnosticRequest, PSTR("/write/diagnostic/%s"), robotId);
    return httpClient.postFile(commandServer, diagnosticRequest, commandServerPort, &diagnosticStream, strlen(diagnostic));
}

void setup(){
    Serial.begin(115200);
    WiFi.init(Serial);
    statusTimer = 0;
}

void loop() {
   int gas = analogRead(A0);
   float hum = dht.readHumidity();
   float temp = dht.readTemperature();
   
   if (millis() - statusTimer > 1*1000) {
        if(connectionOK) {
            statusTimer = millis();
            connectionOK = reportDiagnostic(gas, hum, temp) != 0x0;
        }                                   
    }
    if (!connectionOK) {
        connectionOK = WiFi.begin(ssid, password) == WL_CONNECTED;
        connectionAttempts++;
        if (connectionOK) {
            connectionOK = reportDiagnostic(gas, hum, temp) != 0;
        }
    } 
}

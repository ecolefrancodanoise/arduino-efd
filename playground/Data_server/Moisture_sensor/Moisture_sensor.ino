#include "HttpStreamClient.h"
#include "wificonfig.h"
#include "StringStream.h"

#ifdef ARDUINO_SAMD_MKRGSM1400
    #include <MKRGSM.h>
    GSMClient tcpClient;
    GPRS gprs;
    GSM gsmAccess;
#elif ARDUINO_SAMD_MKRNB1500
    #include <MKRGSM.h>
    NBClient tcpClient;
    GPRS gprs;
    NB nbAccess;
#elif defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
    #include <WiFiNINA.h>
    WiFiClient tcpClient;
#else
    #include "EspClient.h"
    HardwareSerial* ss = &Serial;
    EspClient tcpClient(ss);
#endif

HttpStreamClient httpClient(&tcpClient);
bool connectionOK = false;

long int statusTimer;
char command[32];
unsigned long commandTimer;
unsigned long commandPeriod = 1000;
int connectionAttempts = 0;

const char* reportDiagnostic() {
    StringStream diagnosticStream;
    char diagnostic[100];
    int uptime = millis() / 1000;
    sprintf_P(diagnostic, PSTR("%d %d\n"), analogRead(0),analogRead(1));
    diagnosticStream.setString(diagnostic);
    char diagnosticRequest[50];
    sprintf_P(diagnosticRequest, PSTR("/write/diagnostic/%s"), robotId);
    return httpClient.postFile(commandServer, diagnosticRequest, commandServerPort, &diagnosticStream, strlen(diagnostic));
}

void setup(){
#if !defined(ARDUINO_ARCH_SAMD) && !defined(ARDUINO_AVR_UNO_WIFI_REV2) && !defined(ARDUINO_SAMD_NANO_33_IOT)
    ss->begin(115200);
#endif
}

void loop() {

    if (millis() - statusTimer > 1000) {
        if(connectionOK) {
            statusTimer = millis();
            connectionOK = reportDiagnostic() != 0x0;
        }
    }
    if (!connectionOK) {
        #ifdef ARDUINO_SAMD_MKRGSM1400
        connectionOK = (gsmAccess.begin(PINNUMBER) == GSM_READY) &&
                       (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY);
        #elif ARDUINO_SAMD_MKRNB1500
        connectionOK = (nbAccess.begin(PINNUMBER) == NB_READY) &&
                       (gprs.attachGPRS(GPRS_APN, GPRS_LOGIN, GPRS_PASSWORD) == GPRS_READY);
        #elif defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
        connectionOK = WiFi.begin(ssid, password) == WL_CONNECTED;
        #else
        connectionOK = tcpClient.begin(ssid, password);
        #endif
        connectionAttempts++;
    }
}

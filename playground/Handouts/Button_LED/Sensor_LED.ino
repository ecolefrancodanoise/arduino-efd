int led =8;  //Marking the pin for the LED
int sensor =2; //Marking the pin for the sensor

int value = 0; //Creating a variable for the value

void setup() {
  
  Serial.begin (9600);  //Serial begining state

  pinMode(led, OUTPUT);  //Setting LED pin as output
  pinMode(sensor, INPUT);  //Setting sensor pin as input
}

void loop() {
  
//value=analogRead(sensor);  //Use if analog sensor
  value=digitalRead(sensor);  //Use if digital sensor
  
  if(value==1){  //Change the value range to fit your need
    digitalWrite(led,HIGH);  //Turning LED on
  }
  else{
    digitalWrite(led,LOW);  //Turning LED off
  }
  Serial.println(value);  //Printing the value of sensor in the monitor 
}  
   

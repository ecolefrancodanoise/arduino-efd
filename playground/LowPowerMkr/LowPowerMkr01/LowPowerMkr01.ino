/**
 * Plain do-nothing code.
 * On reset current raises to ~40-50 mA and stabilizes after a couple of seconds at 23.5 +/- 0.1
 */

void setup() {}

void loop() {}

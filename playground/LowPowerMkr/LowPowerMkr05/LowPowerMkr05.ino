/**
 * Plain do-nothing code but create a GSM instance and call gsmAccess.begin() followed by a call to gsmAccess.shutdown()
 * Also call LowPower.deepSleep(30000);
 *
 * On reset current raises to ~40-150 mA and stabilizes after 10-20 seconds at varying levels, for instance
 *  * 14.0 mA +/- 0.1 Then raises to 23.3 +/- 0.1 (at ~45 secs) (2 samples)
 *  * 20.8 mA +/- 0.1 (2 samples)
 *  * 20.5 mA +/- 0.1 (2 samples)
 *  * 13.9 mA +/- 0.1 Then raises to 24.2 +/- 0.1 (at ~45 secs) (1 samples)
 *  * 21.0 mA +/- 0.1 (1 samples)
 *  * 20.4 mA +/- 0.1 (1 samples)
 *  * 14.0 mA +/- 0.1 Then raises to 24.4 +/- 0.1 (at ~45 secs) (1 samples)
 
 */

#include <MKRGSM.h>
#include <ArduinoLowPower.h>

GSM gsmAccess;

void setup() {
    gsmAccess.begin();
    gsmAccess.shutdown();
    LowPower.deepSleep(30000);
}

void loop() {
}

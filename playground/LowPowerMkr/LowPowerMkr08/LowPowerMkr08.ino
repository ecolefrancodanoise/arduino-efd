/**
 * Attempt at powering the GPS from a digital pin. Doesn't work, unsurprisingly.
 * The GPS draws ~40 mA
 */

int gpsPowerPin = 7;
void setup() {
    pinMode(gpsPowerPin, OUTPUT);
    digitalWrite(gpsPowerPin, HIGH);
    delay(10000);
    digitalWrite(gpsPowerPin, LOW);
}

void loop() {}

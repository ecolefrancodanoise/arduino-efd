/*
 * A remote controlled rover using the Qt remote controller
 */

#include "olimexcontroller.h"
#if defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
    #include <WiFiNINA.h>
#else
    #include "WiFiEspAT.h"
#endif

OlimexController olimexController(3, 4, 5, 6, 7, 8);
WiFiServer tcpServer(3000);
WiFiClient client;

const char ssid[] = "roverrc";
float values[4] = {0.0, 0.0, 0.0, 0.0};

void setup() {
    olimexController.setup();
    Serial.begin(115200);
#if !defined(ARDUINO_AVR_UNO_WIFI_REV2) && !defined(ARDUINO_SAMD_NANO_33_IOT)
    WiFi.init(Serial);
#endif
    WiFi.beginAP(ssid);
    tcpServer.begin();
}

void loop() {
    if (WiFiClient newClient = tcpServer.available()) {
        client = newClient;
    }
    uint8_t tcpRxBuffer[296];
    const int nbBytesRead = client.read(tcpRxBuffer, 296);
    for (int i = 0; i < nbBytesRead; i += 2) {
        Serial.println(String() + i + " " + tcpRxBuffer[i] + " " + tcpRxBuffer[i+1]);
        if (tcpRxBuffer[i] >=0 && tcpRxBuffer[i] < 4) {
            values[tcpRxBuffer[i]] = tcpRxBuffer[i+1];
        }
    }
    olimexController.setPower(0, 2 * values[3]);
    olimexController.setPower(1, 2 * values[1]);
}

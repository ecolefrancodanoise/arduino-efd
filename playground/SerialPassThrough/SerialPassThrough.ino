#include "SoftwareSerial.h"

SoftwareSerial* ss = new SoftwareSerial(8, 9);
void setup(){
    Serial.begin(9600);
    ss->begin(9600);
}

void loop() {
    if (Serial.available()) {
        ss->print((char)Serial.read());
    }
    if (ss->available()) {
        Serial.print((char)ss->read());
    }

}

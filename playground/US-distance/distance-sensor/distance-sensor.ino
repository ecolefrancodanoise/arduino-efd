#include <Servo.h>
#include "distance-scanner.h"
int servoPin = 9;
Servo myservo;

int inputPin = 4;
int outputPin = 5;

void setup() {
  Serial.begin(115200 );
  pinMode(inputPin,INPUT);
  pinMode(outputPin,OUTPUT);
  myservo.attach(servoPin);
}

void loop(){
  char ms[200];
  ms[0]=0;
  scandistances(inputPin,outputPin,myservo,ms);
  Serial.println(ms);
}

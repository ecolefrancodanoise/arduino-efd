
#include <Arduino_LSM6DS3.h>
#include "BasicLinearAlgebra.h"

using BLA::Matrix;

Matrix <3> operator*(float c, Matrix <3> M) {
    return Matrix <3> ({c*M(0), c*M(1), c*M(2)});
}

Matrix <3> ypr;
Matrix <3> gyro;

void setup() {
    Serial.begin(115200);
    IMU.begin();
}

void loop() {
    if (IMU.accelerationAvailable() && IMU.gyroscopeAvailable()) {
        Matrix <3> acceleration;
        Matrix <3> gyroReading;
        IMU.readAcceleration(acceleration(1), acceleration(0), acceleration(2)); //mapping (x, y, z) to (fwd, right, down)
        IMU.readGyroscope(gyroReading(1), gyroReading(2), gyroReading(0)); //mapping (gy1, gy2, gy3) to (yaw CCW, pitch nose down, roll right)
        gyroReading(2) = - gyroReading(2);
        gyro = 0.4*gyroReading + 0.6 * gyro; //low pass filter angular velocity
        Matrix <3> yprFromGravity = {0.0, atan2(acceleration(0), acceleration(2))*RAD_TO_DEG, atan2(acceleration(1), acceleration(2))*RAD_TO_DEG}; // ignore yaw (set to 0)
        const float sampleTime = 1/104;
        ypr += gyro*sampleTime;
        ypr = 0.95 * ypr + 0.05 * yprFromGravity; //complementary filter
        Serial.print("acc: fwd: ");
        Serial.print(acceleration(0));
        Serial.print("\tright: ");
        Serial.print(acceleration(1));
        Serial.print("\tdown: ");
        Serial.print(acceleration(2));
        Serial.print("\tgyro: yaw CCW: ");
        Serial.print(gyroReading(0));
        Serial.print("\tpitch down: ");
        Serial.print(gyroReading(1));
        Serial.print("\troll right: ");
        Serial.print(gyroReading(2));
        Serial.print("\tangle: yaw CCW: ");
        Serial.print(ypr(0));
        Serial.print("\tpitch down: ");
        Serial.print(ypr(1));
        Serial.print("\troll right: ");
        Serial.print(ypr(2));
        Serial.println();
    }
}

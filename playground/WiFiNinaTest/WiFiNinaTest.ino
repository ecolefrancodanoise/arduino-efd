/*
 * A test for a WifiNina connection
 *
 * In the terminal:
stty -icanon
xset r rate 160 30
nc 192.168.4.1 3000
 *      
 */

#include <WiFiNINA.h>

WiFiServer tcpServer(3000);
WiFiClient client;

void setup() {
    Serial.begin(115200);
    WiFi.beginAP("wifininatest");
    tcpServer.begin();
}

void loop() {
    if (WiFiClient newClient = tcpServer.available()) {
        client = newClient;
    }
    if (client && client.available()) {
        const char input = client.read();
        const String inputStr = String(input); //need to allocate a String variable to store the subsequent c_str
        const char* outputString = inputStr.c_str();
        Serial.print(outputString);
        client.write(outputString, strlen(outputString));
    }
}

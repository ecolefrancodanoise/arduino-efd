/*
 * A small program to see if there are recognizable patterns when sampling
 * an analog input.
 *
 * Try with different antennas on the analog pin.
 */

#define ROWS 20
#define COLUMNS 20

int measurements[ROWS][COLUMNS];

void setup() {

    Serial.begin(115200);
    int t1 = millis();
    for (int i = 0; i < ROWS; ++i) {
        for (int j = 0; j < COLUMNS; ++j) {
            measurements[i][j] = analogRead(A5);
        }
    }
    int t2 = millis();
    for (int i = 0; i < ROWS; ++i) {
        for (int j = 0; j < COLUMNS; ++j) {
            char buffer[10];
            sprintf(buffer, "%4d", measurements[i][j]/100);
            Serial.print(buffer);
        }
        Serial.println();
    }
        Serial.println();
        Serial.print("millis: ");
        Serial.println(t2 - t1);
}

void loop() {}

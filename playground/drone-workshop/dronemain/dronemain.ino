/*
 * A drone using an Arduino Uno
 */

#if defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
    #include "radiocontrollerwifinina.h"
    #include "gyrolsm6ds3.h"
    const int motorPins[] = {5, 6, 9,10};
    // const float Kp = 1, Kd = 0.6; //Wifi Uno rev2
    // const float Kp = 2, Kd = 1.2; //Wifi Uno rev2
    const float Kp = 4, Kd = 1; //Wifi Uno rev2

#else
    #include "radiocontroller.h"
    #include "gyro.h"
    const int motorPins[] = {3, 9, 10, 11};
    //const float Kp = 100, Kd = 1.2; //F-450 frame
    const float Kp = 15, Kd = 0.3; //Racer drone
#endif
#include "pidcontroller.h"
#include "motors.h"
#include "droneautopilot.h"

bool isArmed = false;
Motors motors(motorPins);
#if defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
    GyroLSM6DS3 gyro;
    RadioControllerWifiNina rc("efdrobot");
#else
    Gyro gyro;
    RadioController rc("efdrobot");
#endif

DroneAutopilot autopilot;

void setup() {
    motors.init();
    gyro.init();
    rc.init();
}

void loop() {
    rc.read();
    if (rc.isArmed && gyro.isReady()) {
        GyroReadings gyroReadings = gyro.read();
        Matrix <3> target = {0.0, 0.0, 0.0};
        int throttle = 0;
        if (autopilot.isActive()) {
            target = autopilot.targetAttitude();
            throttle = autopilot.throttle();
        } else {
            target = {rc.yaw() , rc.pitch(), rc.roll()}; //Note: for yaw, joystick control indicates relative change
            throttle = map(rc.throttle(), -127, 128, 0, 1000);
        }
        PidController pidController({Kp, Kd, gyroReadings.yprMat, gyroReadings.gyro, target});
        motors.activate(pidController.motorPowers(), throttle);
        float rcValues[] = {rc.yaw(), rc.pitch(), rc.roll(), rc.throttle()};
        rc.write(gyroReadings, pidController.motorPowers(), throttle, rcValues);
    }
}

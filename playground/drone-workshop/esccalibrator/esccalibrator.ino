/**
 * Utility program to calibrate 4 servos connected to an Arduino with PWM ports. Tested on Simonk 30A
 * To calibrate:
 *  1. connect the Arduino with this program to PC via USB
 *  2. send the character 9 (max pulse length)
 *  3. connect the LiPo battery to the drone (i.e. to the ESCs)
 *  4. within 2 seconds, send 0 over the serial connection (min pulse length)
 *  5. you're done. Try and test the ESCs by sending values 1..9 over the serial connection
 * send characters 0-9 (min, max throttle)
 */
#include <Servo.h>
#define MIN_PULSE_LENGTH 1000 // Minimum pulse length in µs
// #define MAX_PULSE_LENGTH 2000 // Maximum pulse length in µs
#define MAX_PULSE_LENGTH 2000 // Maximum pulse length in µs
Servo motors[4];
#if defined(ARDUINO_AVR_UNO_WIFI_REV2) || defined(ARDUINO_SAMD_NANO_33_IOT)
    const int motorPins[] = {5, 6, 9,10};
#else
    const int motorPins[] = {3, 9, 10, 11};
#endif

void setup() {
    Serial.begin(115200);
    for (int i = 0; i < 4; ++i) {
        motors[i].attach(motorPins[i], MIN_PULSE_LENGTH, MAX_PULSE_LENGTH);
        motors[i].writeMicroseconds(MAX_PULSE_LENGTH);
    }
}

void loop() {
    const int interval = (MAX_PULSE_LENGTH - MIN_PULSE_LENGTH) / 9;
    if (Serial.available()) {
        const char data = Serial.read();
        if(data >=48 && data < 58) {
            const int pulseLength = (data-48) * interval + MIN_PULSE_LENGTH;
            Serial.print("pulseLength in µs: ");
            Serial.println(pulseLength);
            for (int i = 0; i < 4; ++i) {
                motors[i].writeMicroseconds(pulseLength);
            }
        }
    }
}

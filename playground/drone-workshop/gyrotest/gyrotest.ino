#include "gyro.h"

Gyro gyro;

void setup() {
    Serial.begin(115200);
    gyro.init();
}

void loop() {
    if (gyro.isReady()) {
        GyroReadings gyroReadings = gyro.read();
        Serial.print(gyroReadings.yprMat(0));
        Serial.print(" ");
        Serial.print(gyroReadings.yprMat(1));
        Serial.print(" ");
        Serial.print(gyroReadings.yprMat(2));
        Serial.print(" ");
        Serial.println("");
    }
}

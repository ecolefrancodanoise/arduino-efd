    /*
 * Test of the LSM6DS3 and a Madgwick filter for calculation yaw, pitch and roll.
 */

#include <Arduino_LSM6DS3.h>
#include <MadgwickAHRS.h>

#define SAMPLE_RATE 100  // in Hz

Madgwick filter;

void setup() {
   Serial.begin(115200);
   IMU.begin();
   filter.begin(SAMPLE_RATE);
}


void loop() {
   float ax, ay, az;
   float gx, gy, gz;
   if (IMU.accelerationAvailable() && IMU.gyroscopeAvailable()) {
        IMU.readAcceleration(ax, ay, az);
        IMU.readGyroscope(gx, gy, gz); // -p' , -r', -y'
        filter.updateIMU(gx, gy, gz, ax, ay, az);
        Serial.println(String(millis()) + " "
            + gx +  " " + gy +  " " + gz + "       "
            + ax + " " + ay + " " + az + "      "
            + filter.getRoll() + " " + filter.getPitch() + " " + filter.getYaw());
    }
}

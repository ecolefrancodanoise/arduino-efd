#include "HttpStreamClient.h"
#include "wificonfig.h"
#include "StringStream.h"
#include "WiFiEspAT.h"
WiFiClient tcpClient;

HttpStreamClient httpClient(&tcpClient);
bool connectionOK = false;

long int statusTimer;
int connectionAttempts = 0;

const char* reportDiagnostic() { 
    StringStream diagnosticStream;
    char diagnostic[100];
    int uptime = millis() / 1000;
    int value = analogRead(A0);
    sprintf_P(diagnostic, PSTR("Upt %d<br>wfi: %d<br>value: %d"), uptime, connectionAttempts,value);
    diagnosticStream.setString(diagnostic);
    char diagnosticRequest[50];
    sprintf_P(diagnosticRequest, PSTR("/write/diagnostic/%s"), robotId);
    return httpClient.postFile(commandServer, diagnosticRequest, commandServerPort, &diagnosticStream, strlen(diagnostic));
}

void setup(){
    Serial.begin(115200);
    WiFi.init(Serial);
    statusTimer = 0;
}

void loop() {
    if (millis() - statusTimer > 1*1000) {
        if(connectionOK) {
            statusTimer = millis();
            connectionOK = reportDiagnostic() != 0x0;
        }
    }
    if (!connectionOK) {
        connectionOK = WiFi.begin(ssid, password) == WL_CONNECTED;
        connectionAttempts++;
        if (connectionOK) {
            connectionOK = reportDiagnostic() != 0;
        }
    } 
}

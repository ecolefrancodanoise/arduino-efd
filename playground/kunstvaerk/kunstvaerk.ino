#include <Servo.h>

int angle = 10;
int outputPin = 2;
int inputPin = 4;
 
Servo myservo; 

void setup() {
  myservo.attach(9);
  Serial.begin(9600);
  pinMode(outputPin, OUTPUT);
  myservo.write(angle);

}

void loop() {
  digitalWrite(outputPin,LOW);
  delayMicroseconds(2);
  digitalWrite(outputPin,HIGH);
  delayMicroseconds(10);
  digitalWrite(outputPin,LOW);
  float pingTime = pulseIn(inputPin,HIGH);
  float distance = pingTime*0.034/2;

  Serial.println(distance);

  if(distance < 30) {
    for(angle = 10; angle < 180; angle++) {
      delay(2);
      myservo.write(angle);
    }

    for(angle = 180; angle > 10; angle--) {
      delay(2);   
      myservo.write(angle);   
    }

  } else  if(distance < 70)  {
    
    for(angle = 10; angle < 110; angle++) {
      delay(8);
      myservo.write(angle);         
    }
    for(angle = 110; angle > 10; angle--)  {                  
      delay(8);
      myservo.write(angle);         
    }
  } else  if(distance < 110)  {
    
    for(angle = 10; angle < 30; angle++) {
      delay(20);
      myservo.write(angle);         
    }
    for(angle = 30; angle > 10; angle--)   {                 
      delay(20);
      myservo.write(angle);        
    }
  }  else {
    myservo.write(90);
  }
}
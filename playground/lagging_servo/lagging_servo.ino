#include <Servo.h>
Servo myservo1;
Servo myservo2;
Servo myservo3;

void setup() {
  Serial.begin(9600);
  myservo1.attach(7);
  myservo2.attach(8);
  myservo3.attach(9);

}
int wantedpos = 50;
int servoangle = 0;
int timer = 0;
void loop() {
  timer = timer +1;
  //Type in number from 0 to 180 to move all servos
  if(Serial.available()) {
    const int userInput = Serial.parseInt();
    Serial.println(userInput);
    if( userInput>0){
      wantedpos = userInput;
      Serial.println("parseint() >10 true");
    }
  }
  if(timer %100 == 0 && wantedpos != servoangle){
    Serial.println("inside if");
    if(servoangle > wantedpos){
      servoangle = servoangle-1;
    }
    
    if(servoangle < wantedpos){
      servoangle = servoangle+1;
    }
  }
  myservo1.write(servoangle);
  myservo2.write(servoangle);
  myservo3.write(servoangle);
}

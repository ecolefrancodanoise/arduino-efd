/*
  * A randomly blinking lightshow governed by a Markov chain
  */
const int nbMarkovStates = 5;
const int markovTransitions[nbMarkovStates][nbMarkovStates] =
{{0,0,0,0,1}, {0,0,0,0,3}, {0,0,0,0,3}, {0,0,0,0,4}, {0,0,0,0,3}};
const bool markovOutputs[nbMarkovStates] = {LOW, LOW, LOW, HIGH};
int currentState = 0;

void setup() {
    pinMode(7, OUTPUT);
}

void loop() {
    currentState =
markovTransitions[currentState][random(0,nbMarkovStates)];
    digitalWrite(7, markovOutputs[currentState]);
    delay(10);
}
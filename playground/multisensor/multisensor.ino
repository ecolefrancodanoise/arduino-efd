#include <TinyDHT.h>
DHT dht(7, DHT22);

void setup() {
  Serial.begin(115200);
}

void loop() {
  int reading = analogRead(A0);
  float hum = dht.readHumidity();
  float temp = dht.readTemperature();
  Serial.println(hum);
  Serial.println(temp);
  Serial.println(reading);
  Serial.println("===========");
  delay(1000);
}

import QtQuick

Rectangle {
    width: 360
    height: 480
    color: "black"

    MultiPointTouchArea {
        objectName: "touchPointArea"
        anchors.fill: parent
        touchPoints: [
            TouchPoint { id: touch1 },
            TouchPoint { id: touch2 }
        ]
    }

    Rectangle {
        width: 30; height: 30
        color: "green"
        x: touch1.x
        y: touch1.y
    }

    Rectangle {
        width: 30; height: 30
        color: "yellow"
        x: touch2.x
        y: touch2.y
    }

    Text {
        text: "Left x:" + touch1.x + ", y: " + touch1.y + "\nRight x:" + touch2.x + ", y: " + touch2.y
        color: "red"
    }
}

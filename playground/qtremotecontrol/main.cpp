#include <QDir>
#include <QGuiApplication>
#include <QQmlEngine>
#include <QQmlFileSelector>
#include <QQuickItem>
#include <QQuickView>
#include "tcpcommandsender.h"

int main(int argc, char* argv[]) {
    QGuiApplication app(argc,argv);
    app.setOrganizationName("L' école franco-danoise");
    app.setOrganizationDomain("ecolefrancodanoise.dk");
    app.setApplicationName(QFileInfo(app.applicationFilePath()).baseName());
    QQuickView view;
    view.connect(view.engine(), &QQmlEngine::quit, &app, &QCoreApplication::quit);
    new QQmlFileSelector(view.engine(), &view);
    view.setSource(QUrl("qrc:///qtremotecontrol/joysticks.qml"));
    QObject *touchPointArea = view.rootObject()->findChild<QObject*>("touchPointArea");
    TcpCommandSender tcpCommandSender(touchPointArea);
    view.show();
    return app.exec();
}

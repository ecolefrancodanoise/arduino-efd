#include <QtQml/qqmlregistration.h>
#include <QTcpSocket>
#include <QTouchEvent>

class TcpCommandSender : public QObject {
    Q_OBJECT

    public:
    TcpCommandSender (QObject* touchPointArea) : tcpSocket(this),
                                                touchAreaHeight(touchPointArea->property("height").toInt()),
                                                touchAreaWidth(touchPointArea->property("width").toInt()) {
        tcpSocket.connectToHost(QHostAddress("192.168.4.1"), 3000);
        outStream.setDevice(&tcpSocket);
        connect(touchPointArea, SIGNAL(updated(const QList <QObject*> &)), this, SLOT(sendMessage(const QList <QObject*> &)));
    }

    public slots:
    void sendMessage(const QList <QObject*> &touchPoints) {
        foreach(QObject* touchPoint, touchPoints) {
            int x = touchPoint->property("x").toInt();
            int y = touchPoint->property("y").toInt();
            qDebug() << "width: " << touchAreaWidth << " rawx " << x << " rawy " << y;
            x = (x - touchAreaWidth / 2) * 128 / (touchAreaWidth / 2);
            if (y < touchAreaHeight / 2) {
                y = (y - touchAreaHeight / 4) * 128 / (touchAreaHeight / 4);
                const signed char bytes[] = {0, (signed char)y, 3, (signed char)x}; //left joystick, yaw and throttle
                outStream.writeRawData((const char*)bytes, 4);
            } else {
                y = (y - 3 * touchAreaHeight / 4) * 128 / (touchAreaHeight / 4);
                const signed char bytes[] = {1, (signed char)x, 2, (signed char)y}; //right joystick, pitch and roll
                outStream.writeRawData((const char*)bytes, 4);
            }
        }
    }

    private:
        QTcpSocket tcpSocket;
        QDataStream outStream;
        const int touchAreaHeight;
        const int touchAreaWidth;

};

from picamera import PiCamera
from picamera.array import PiRGBArray
import numpy as np
from time import sleep
from matplotlib import pyplot as plt

with PiCamera() as camera:
    camera.resolution = (2592,1952)
    rawCapture = PiRGBArray(camera)
    sleep(0.1)
    camera.capture(rawCapture, format='rgb')
    image = rawCapture.array

img = np.array(image)

#img[:,:,1] = 0
#img[:,:,0] = 0
plt.imshow(img)
plt.show()


#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdint.h>

#include "calc-ypr.h"

  /*
    Data is written to the FIFO in order of register number (from
    lowest to highest). If all the FIFO enable flags (see below) are
    enabled and all External Sensor Data registers (Registers 73 to
    96) are associated with a Slave device, the contents of registers
    59 through 96 will be written in order at the Sample Rate.
  */

uint8_t dmpdecodequat(int16_t *data, const uint8_t* packet) {
  // if (packet == 0) packet = dmpPacketBuffer;
  data[0] = ((packet[0] << 8) + packet[1]);
  data[1] = ((packet[4] << 8) + packet[5]);
  data[2] = ((packet[8] << 8) + packet[9]);
  data[3] = ((packet[12] << 8) + packet[13]);
  return 0;
}

uint8_t dmpgetquat(struct quat *q, const uint8_t* packet) {
  int16_t qI[4];
  uint8_t status = dmpdecodequat(qI, packet);
  if (status == 0) {
    q -> w = (double)qI[0] / 16384.0f;
    q -> x = (double)qI[1] / 16384.0f;
    q -> y = (double)qI[2] / 16384.0f;
    q -> z = (double)qI[3] / 16384.0f;
    return 0;
  }
  return status; // int16 return value, indicates error if this line is reached
}

uint8_t calcypr(double *data, struct quat *q, struct vecdouble *gravity) {
    // yaw: (about Z axis)
    data[0] = atan2(2*q->x*q->y - 2*q->w*q->z, 2*q->w*q->w + 2*q->x*q->x - 1);
    // pitch: (nose up/down, about Y axis)
    data[1] = atan(gravity->x / sqrt(gravity->y*gravity->y + gravity->z*gravity->z));
    // roll: (tilt left/right, about X axis)
    data[2] = atan(gravity->y / sqrt(gravity->x*gravity->x + gravity->z*gravity->z));
    return 0;
}

uint8_t calcgrav(struct vecdouble *v, struct quat *q) {
    v->x = 2 * (q->x*q->z - q->w*q->y);
    v->y = 2 * (q->w*q->x + q->y*q->z);
    v->z = q->w*q->w - q->x*q->x - q->y*q->y + q->z*q->z;
    return 0;
}

#ifndef CALCYPR_H_
#define CALCYPR_H_

/* struct quat; */

/* struct vecdouble; */

struct quat {
    double w;
    double x;
    double y;
    double z;
};

struct vecdouble {
  double x;
  double y;
  double z;
};

uint8_t dmpdecodequat(int16_t *data, const uint8_t* packet);

uint8_t dmpgetquat(struct quat *q, const uint8_t* packet);

uint8_t calcypr(double *data, struct quat *q, struct vecdouble *gravity);

uint8_t calcgrav(struct vecdouble *v, struct quat *q);

#endif // CALCYPR_H_

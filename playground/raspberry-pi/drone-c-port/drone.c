/*
 * A drone using a raspberry pi zero and a PID controller (without the I for now)
 *
 * Set up the joystick to send via netcat (extract the channel and value positions, then send):
 *  cat  /dev/input/event20 | stdbuf -o0 xxd -c 24 -ps | stdbuf -o0 cut -c33,34,37,38,41,42 | stdbuf -o0 grep -v "^00" | stdbuf -o0 xxd -r -p | nc raspberrypi.local 7891
 *
 * Motor configuration
 * forward
 *  0(CW)  1(CCW)
 *  3(CCW) 2(CW)
 * back
 */

/* To build: make drone */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

/* GPIO */
#include <pigpio.h>

/* Server */
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>

#include <string.h>

/* Math */
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

/* I2C + MPU6050 */
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>

#include <stdbool.h>

#include "dmp.h"
#include "mpu.h"
#include "i2c.h"
#include "calc-ypr.h"

const double Kp = 40; const double Kd = 0.8;
int armed = 0;

/* const int base_pw = 1000; */
/* const int base_pw = 650; */
const int base_pw = 900;

int throttle;

const int pins[] = {18,17,27,22};


/*
     (18)     (gyro)     (17)







     (22)                (27)
 */


int run=1;

int width = 1100;
int pin = 18;

int presocket, newsocket;
/* char buffer[1024]; */
/* char buffer[296]; */
uint8_t buffer[296];
struct sockaddr_in server;
struct sockaddr_storage serverutorage;
socklen_t addr_size;

double erroractarr[12] = {1, -1, -1, -1, -1, 1, 1, 1, 1, -1, 1, -1};

int8_t ctrl_values[4] = {0, 0, 0, 0}; // Note: for yaw, joystick control
                                      // indicates relative change

double motorpwrarr[4];

/* const int gyroarr[3] = {-gy.z, -gy.y, gy.x}; */
/* double gyroarr[3]; */

double prevgyroarr[3] = {0, 0, 0};
double prevyprarr[3] = {0, 0, 0};
double yprarr[3];


struct quat q;           // [w, x, y, z]         quaternion container

struct vecdouble grav;

long map(long x, long in_min, long in_max, long out_min, long out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void serverinit() {
  presocket = socket(PF_INET, SOCK_STREAM, 0);
  /* Set socket state to nonblocking, so drone doesnt freeze when no
   instructions are received. */
  fcntl(presocket, F_SETFL, O_NONBLOCK);

  server.sin_family = AF_INET;
  server.sin_port = htons(7891);
  server.sin_addr.s_addr = INADDR_ANY; // tcp socket @ localhost
  /* Zeropad address field */
  bzero(&(server.sin_zero), 8);

  if (0 == bind(presocket, (struct sockaddr *) &server, sizeof(server)))
    printf("Bound socket\n");
  else
    printf("Error: Socket bind failure\n");

  if (listen(presocket, 5) == 0)
    printf("Listening\n");
  else
    printf("Error\n");

  printf("Conn\n");
}

#define PI 3.14159265


uint16_t merge_bytes( uint8_t LSB, uint8_t MSB) {
  return  (uint16_t) ((( LSB & 0xFF) << 8) | MSB);
}

// 16 bits data on the MPU6050 are in two registers,
// encoded in two complement. So we convert those to int16_t
int16_t two_complement_to_int( uint8_t LSB, uint8_t MSB) {
  int16_t signed_int = 0;
  uint16_t word;

  word = merge_bytes(LSB, MSB);

  if((word & 0x8000) == 0x8000) { // negative number
        signed_int = (int16_t) -(~word);
    } else {
        signed_int = (int16_t) (word & 0x7fff);
    }

    return signed_int;
}

char accel_x_h,accel_x_l;
uint16_t fifo_len = 0;
float x_gyro_g, y_gyro_g, z_gyro_g;

void stop(int signum)
{
   run = 0;
}

int main(int argc, char *argv[]) {

  /* INITIALIZE SERVER */

  serverinit();

  /* INITIALIZE MOTORS */
  if (gpioInitialise() < 0)
    return -1;

  gpioSetSignalFunc(SIGINT, stop);

  printf("\nMOTORINIT\n");

  for (int i = 0; i < 4; ++i) {
    printf("M%d, %d\n", pins[i], 1000);
    gpioServo(pins[i], 1000);
  }

  sleep(2);

  /* /\* Test Motors *\/ */
  /* printf("\nMOTORTEST\n"); */

  /* for (int i = 0; i < 4; ++i) { */
  /*   printf("M%d, %d\n", pins[i], 1000); */
  /*   gpioServo(pins[i], 1200); */
  /* } */

  /* sleep(1); */

  printf("\nBASEPW\n");

  for (int i = 0; i < 4; ++i) {
    printf("M%d, %d (base_pw)\n", pins[i], base_pw);
    gpioServo(pins[i], base_pw);
  }

  sleep(2);

  int adapter_nr = 1; // Device ID
  char bus_filename[250];

  snprintf(bus_filename, 250, "/dev/i2c-%d", adapter_nr);
  file = open(bus_filename, O_RDWR);
  if (file < 0) {
    fprintf(stderr, "Failed to open device: %s\n", strerror(errno));
    exit(1);
  }

  if (ioctl(file, I2C_SLAVE, MPU6050_I2C_ADDR) < 0) {
    fprintf(stderr, "Failed to select device: %s\n", strerror(errno));
    close(file);
    exit(1);
  }

  /* dmpInitialize begin */
  i2c_writebit(REG_PWR_MGMT_1, 7, true); // reset dmp
  usleep(30000); // wait after reset
  i2c_writebit(REG_PWR_MGMT_1, 6, false); // Disable sleep mode

  /* i2c_writebit(REG_PWR_MGMT_1, 7, true); // reset dmp */

  /* Selecting user bank 16... */
  MPUSetMemoryBank(0x10, true, true);
  /* Selecting memory byte 6... */
  MPUSetMemoryStartAddress(0x06);
  /* Resetting memory bank selection to 0... */
  MPUSetMemoryBank(0, false, false);

  if (MPUWriteMemoryBlock(dmpMemory, MPU6050_DMP_CODE_SIZE, 0, 0, true)) {
    printf("Success! DMP code written and verified.\n");
  } else {
    printf("FAILURE\n");
  }

  if (MPUWriteDMPConfig(dmpConfig, MPU6050_DMP_CONFIG_SIZE)) {
    printf("Success! DMP configuration written and verified.\n");
  } else {
    printf("FAILURE\n");
  }

  /* Setting gyro sensitivity to +/- 2000 deg/sec... */
  i2c_writebits(0x1B, 4, 2, 0x03);

  MPUSetDMPConf1(0x03);
  MPUSetDMPConf2(0x00);

  MPUResetFIFO();
  MPUSetFIFOEnabled(true);
  MPUSetDMPEnabled(true);
  /* dmpInitialize done */

  usleep(100000);

  double gyroarr[] = {-z_gyro_g, -y_gyro_g, x_gyro_g};
  double targetarr[] = {ctrl_values[0], ctrl_values[1], ctrl_values[2]};

  /* Initialize GSL vector views */
  gsl_vector_view target = gsl_vector_view_array(targetarr, 3);
  gsl_vector_view gyro = gsl_vector_view_array(gyroarr, 3);
  gsl_vector_view prevgyro = gsl_vector_view_array(prevgyroarr, 3);
  gsl_vector_view ypr = gsl_vector_view_array(yprarr, 3);
  gsl_vector_view prevypr = gsl_vector_view_array(prevyprarr, 3);

  gsl_matrix_view erroract = gsl_matrix_view_array(erroractarr, 4, 3);
  gsl_matrix_view gyromat = gsl_matrix_view_array(gyroarr, 3, 1);
  gsl_matrix_view motorpowers = gsl_matrix_view_array(motorpwrarr, 4, 1);

  /* RUN */
  while (run) {

    /* Accept incoming connections */
    int s;
    if (!newsocket || newsocket == -1 || s == 0) {
      addr_size = sizeof serverutorage;
      newsocket = accept(presocket, (struct sockaddr *)&serverutorage, &addr_size);
      fcntl(newsocket, F_SETFL, O_NONBLOCK);
    }

    /* Read incoming instruction */
    s = recv(newsocket, buffer, 296, 0);

    if (!(s == 0 || s < 0)) {
      printf("Received: %s\n\n", buffer);

      int runs = 0;
      for (int i = 0; i < s; i += 3) {
        printf(">: %d %d\n", buffer[i + 1], buffer[i + 2]);
        /* ctrl_values[buffer[i + 1]] = buffer[i + 2]; */
        runs++;
      }

      printf("Received: %s (size: %d)\n\n", buffer, s);
      printf("Ctrl Values: %d %d %d %d (runs %d)\n\n",
             ctrl_values[0], ctrl_values[1], ctrl_values[2], ctrl_values[3], runs);

      memset(buffer, 0, sizeof(buffer));
    }

    if (armed != 1) {
      if (ctrl_values[0] < -100 && ctrl_values[1] < -100 &&
          ctrl_values[2] < -100 && ctrl_values[3] < -100) {
        armed = 1;
      }
    }

    armed = 1;

    int ready = i2c_read(REG_INT_STAT);
    /* printf("%d ", ready & 1); */

    if(!((ready & 1) == 1)){
      printf("NOT READY\n");
      /* sleep(1); */
      continue;
    }

    uint8_t fifobuf[64]; // FIFO storage buffer

    /* get fifo buffer length */
    accel_x_h = i2c_read(REG_FIFO_COUNT_L);
    accel_x_l = i2c_read(REG_FIFO_COUNT_H);
    fifo_len = merge_bytes(accel_x_h, accel_x_l);

    if (fifo_len == 1024) {
      i2c_writebit(REG_USER_CTRL, 2, true); // reset fifo
      printf("FIFO OVERFLOW\n");
      continue;
    }

    if (fifo_len >= DMP_PACKET_SIZE) {

      /* printf("Reading MPU\n\n"); */

      i2c_readbytes(REG_FIFO, DMP_PACKET_SIZE, fifobuf); // read fifo buffer

      dmpgetquat(&q, fifobuf);

      calcgrav(&grav, &q);
      calcypr(yprarr, &q, &grav);



      printf("accel: x %7.3fg   y %7.3fg   z %7.3fg\t",
             q.x, q.y, q.z);
      printf("gyro: x %7.3f°/s   y %7.3f°/s   z %7.3f°/s\t",
             grav.x, grav.y, grav.z);
      /* printf("ypr: %.3f %.3f %.3f", yprarr[0], yprarr[1], yprarr[2]); */
      printf("ypr  %7.2f %7.2f %7.2f", yprarr[0]*180/PI, yprarr[1]*180/PI, yprarr[2]*180/PI);

      /* gsl_blas_daxpy(-1, &target.vector, &gyro.vector); */

      gsl_vector_scale(&ypr.vector, 0.2);
      gsl_vector_scale(&prevypr.vector, 0.8);
      gsl_vector_add(&ypr.vector, &prevypr.vector);

      /*
       gyro = 0.6 * gyro + 0.4 * prevgyro
       low-pass filter the derivative
      */
      gsl_vector_scale(&gyro.vector, 0.4);
      gsl_vector_scale(&prevgyro.vector, 0.6);
      gsl_vector_add(&gyro.vector, &prevgyro.vector);

      int i;

      for (i = 0; i < 3; i++) {
        prevgyroarr[i] = gyroarr[i];
        prevyprarr[i] = yprarr[i];
      }

      /*
       motorpowers = errortoaction * (Kp * yprMat + Kd * gyro - target)
      */
      gsl_vector_scale(&gyro.vector, Kd);
      gsl_vector_scale(&ypr.vector, Kp);
      gsl_vector_add(&gyro.vector, &ypr.vector);
      gsl_vector_sub(&gyro.vector, &target.vector);

      gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, &erroract.matrix,
                     &gyromat.matrix, 0.0, &motorpowers.matrix);

      throttle = map(ctrl_values[3], -128, 127, 0, 1000) + base_pw;

      if (armed == 1) {
        for (int i = 0; i < 4; ++i) {
          gpioServo(pins[i], motorpwrarr[i] + throttle);
          printf(" %f", motorpwrarr[i] + throttle);
        }

      }
      prevgyro = gyro;

      printf("\n");

      usleep(10000);

    } else {
      usleep(10000);
    }
  }

  /* SHUTDOWN */
  printf("\n\nTidying up\n");

  for (int i = 0; i < 4; ++i) {
    gpioServo(pins[i], 1000); // 0 or 1000?
    printf("M%d, %d\n", pins[i], 1000);
  }

  gpioTerminate();

  close(file);

  return 0;
}

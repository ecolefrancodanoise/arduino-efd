/*
 * Drone ESC calibration program
 *
 * Motor configuration
 * forward
 *  0(CW)  1(CCW)
 *  3(CCW) 2(CW)
 * back
 */

/*
     (18)     (gyro)     (17)







     (22)                (27)
 */

/* gcc esc-calibrate.c -o esc-calibrate -lm -lpigpio */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

/* GPIO */
#include <pigpio.h>

const char ssid[] = "efdrobot";
const double Kp = 40; const double Kd = 0.8;
int armed = 0;

const int base_pw = 1000;

int throttle;

const int pins[] = {18,17,27,22};

int run=1;

int width = 1100;
int pin = 18;

void stop(int signum)
{
   run = 0;
}

int main(int argc, char *argv[]) {

  /* INITIALIZE MOTORS */
  if (gpioInitialise() < 0)
    return -1;

  gpioSetSignalFunc(SIGINT, stop);

  printf("\nSENDING PW: 2000 (max)\n");

  for (int i = 0; i < 4; ++i) {
    printf("M%d\n", pins[i]);
    gpioServo(pins[i], 2000);
  }

  printf("\nConnect Battery\n");

  sleep(10);

  printf("\nSENDING PW: 1000 (min)\n");

  for (int i = 0; i < 4; ++i) {
    printf("M%d\n", pins[i]);
    gpioServo(pins[i], 1000);
  }

  sleep(10);

  /* SHUTDOWN */
  printf("\n\nTidying up\n");

  for (int i = 0; i < 4; ++i) {
    gpioServo(pins[i], 0); // 0 or 1000?
    printf("M%d\n", pins[i]);
  }

  gpioTerminate();

  return 0;
}

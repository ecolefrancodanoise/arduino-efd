/*
 * A drone using a raspberry pi zero using a PID controller (without the I for now)
 * To debug over wifi, connect to drone via its AP:
 *   nc 192.168.4.1 3000
 *
 * Set up the joystick to send via netcat (extract the channel and value positions, then send):
 *  cat  /dev/input/event20 | stdbuf -o0 xxd -c 24 -ps | stdbuf -o0 cut -c33,34,37,38,41,42 | stdbuf -o0 grep -v "^00" | stdbuf -o0 xxd -r -p | nc 192.168.4.1 3000
 *
 * Motor configuration
 * forward
 *  0(CW)  1(CCW)
 *  3(CCW) 2(CW)
 * back
 */

/* gcc darksidedrone.c -o darksidedrone -lm -lgsl -lgslcblas -Wall -pthread -lpigpio */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

/* GPIO */
#include <pigpio.h>

/* Server */
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>

#include <string.h>

/* Math */
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

/* I2C + MPU6050 */
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>

#include <stdbool.h>
#include "dmp.h"

const char ssid[] = "efdrobot";
const double Kp = 40; const double Kd = 0.8;
int armed = 0;

/* const int base_pw = 1000; */
/* const int base_pw = 650; */
const int base_pw = 900;

int throttle;

const int pins[] = {18,17,27,22};


/*
     (18)     (gyro)     (17)







     (22)                (27)
 */


int run=1;

int width = 1100;
int pin = 18;

int presocket, newsocket;
/* char buffer[1024]; */
char buffer[296];
struct sockaddr_in server;
struct sockaddr_storage serverutorage;
socklen_t addr_size;

double erroractarr[12] = {1, -1, -1, -1, -1, 1, 1, 1, 1, -1, 1, -1};

double ctrl_values[4] = {0, 0, 0, 0}; // Note: for yaw, joystick control
                                      // indicates relative change

double motorpwrarr[4];

/* const int gyroarr[3] = {-gy.z, -gy.y, gy.x}; */
/* double gyroarr[3]; */

double prevgyroarr[3] = {0, 0, 0};
double prevyprarr[3] = {0, 0, 0};

/* double yprarr[3]; */

/*
  MPU6050 i2c reading adapted from:
  https://openest.io/en/services/mpu6050-accelerometer-on-raspberry-pi

  and

  https://github.com/richardghirst/PiBits/blob/master/MPU6050-Pi-Demo/I2Cdev.cpp
 */

int file = -1;


// Please note, this is not the recommanded way to write data
// to i2c devices from user space.
void i2c_write(__u8 reg_address, __u8 val) {
    char buf[2];
    if(file < 0) {
        printf("Error, i2c bus is not available\n");
        exit(1);
    }

    buf[0] = reg_address;
    buf[1] = val;

    if (write(file, buf, 2) != 2) {
        printf("Error, unable to write to i2c device\n");
        exit(1);
    }

}

// Please note, this is not thre recommanded way to read data
// from i2c devices from user space.
char i2c_read(uint8_t reg_address) {
    char buf[1];
    if(file < 0) {
        printf("Error, i2c bus is not available\n");
        exit(1);
    }

    buf[0] = reg_address;

    if (write(file, buf, 1) != 1) {
        printf("Error, unable to write to i2c device\n");
        exit(1);
    }


    if (read(file, buf, 1) != 1) {
        printf("Error, unable to read from i2c device\n");
        exit(1);
    }

    return buf[0];

}

/* bool I2Cdev::writeBit(uint8_t devAddr, uint8_t regAddr, uint8_t bitNum, uint8_t data) { */
/*     uint8_t b; */
/*     i2c_read(devAddr, regAddr, &b); */
/*     b = (data != 0) ? (b | (1 << bitNum)) : (b & ~(1 << bitNum)); */
/*     return writeByte(devAddr, regAddr, b); */
/* } */

bool i2c_writebytes(uint8_t devAddr, uint8_t regAddr, uint8_t length, uint8_t* data) {
    int8_t count = 0;
    uint8_t buf[128];
    int fd;

    if (length > 127) {
        fprintf(stderr, "Byte write count (%d) > 127\n", length);
        return false;
    }

    fd = open("/dev/i2c-1", O_RDWR);
    if (fd < 0) {
        fprintf(stderr, "Failed to open device: %s\n", strerror(errno));
        return false;
    }
    if (ioctl(fd, I2C_SLAVE, devAddr) < 0) {
        fprintf(stderr, "Failed to select device: %s\n", strerror(errno));
        close(fd);
        return false;
    }
    buf[0] = regAddr;
    memcpy(buf+1,data,length);
    count = write(fd, buf, length+1);
    if (count < 0) {
        fprintf(stderr, "Failed to write device(%d):\n", count);
        close(fd);
        return false;
    } else if (count != length+1) {
        fprintf(stderr, "Short write to device, expected %d, got %d\n", length+1, count);
        close(fd);
        return false;
    }
    close(fd);

    return true;
}

int8_t i2c_readbytes(uint8_t devAddr, uint8_t regAddr, uint8_t length, uint8_t *data) {
    int8_t count = 0;
    int fd = open("/dev/i2c-1", O_RDWR);

    if (fd < 0) {
        fprintf(stderr, "Failed to open device: %s\n", strerror(errno));
        return(-1);
    }
    if (ioctl(fd, I2C_SLAVE, devAddr) < 0) {
        fprintf(stderr, "Failed to select device: %s\n", strerror(errno));
        close(fd);
        return(-1);
    }
    if (write(fd, &regAddr, 1) != 1) {
        fprintf(stderr, "Failed to write reg: %s\n", strerror(errno));
        close(fd);
        return(-1);
    }
    count = read(fd, data, length);
    if (count < 0) {
        fprintf(stderr, "Failed to read device(%d):\n", count);
        close(fd);
        return(-1);
    } else if (count != length) {
        fprintf(stderr, "Short read to device, expected %d, got %d\n", length+1, count);
        close(fd);
        return(-1);
    }
    close(fd);

    return count;
}


#define MPU6050_I2C_ADDR            0x68
#define MPU6050_RA_BANK_SEL         0x6D
#define MPU6050_RA_MEM_START_ADDR   0x6E

void MPUSetMemoryBank(uint8_t bank) {
  bank &= 0x1F;
  i2c_write(MPU6050_RA_BANK_SEL, bank);
}

void MPUSetMemoryStartAddress(uint8_t address) {
  i2c_write(MPU6050_RA_MEM_START_ADDR, address);
}

#define MPU6050_DMP_MEMORY_CHUNK_SIZE   16
#define MPU6050_RA_MEM_R_W          0x6F

#define pgm_read_byte(p) (*(uint8_t *)(p))

bool writeMemoryBlock(const uint8_t *data, uint16_t dataSize, uint8_t bank, uint8_t address, bool verify) {
  uint8_t chunkSize, *verifyBuffer;
  uint8_t *progBuffer = NULL; // Keep compiler quiet
  uint16_t i; uint8_t j;

  MPUSetMemoryBank(bank);
  MPUSetMemoryStartAddress(address);

  bool useProgMem = true;
  if (verify) verifyBuffer = (uint8_t *)malloc(MPU6050_DMP_MEMORY_CHUNK_SIZE);
  if (useProgMem) progBuffer = (uint8_t *)malloc(MPU6050_DMP_MEMORY_CHUNK_SIZE);
  for (i = 0; i < dataSize;) {
    // determine correct chunk size according to bank position and data size
    chunkSize = MPU6050_DMP_MEMORY_CHUNK_SIZE;

    // make sure we don't go past the data size
    if (i + chunkSize > dataSize) chunkSize = dataSize - i;

    // make sure this chunk doesn't go past the bank boundary (256 bytes)
    if (chunkSize > 256 - address) chunkSize = 256 - address;

    if (useProgMem) {
      // write the chunk of data as specified
      for (j = 0; j < chunkSize; j++) progBuffer[j] = pgm_read_byte(data + i + j);
    } else {
      // write the chunk of data as specified
      progBuffer = (uint8_t *)data + i;
    }

    i2c_writebytes(MPU6050_I2C_ADDR, MPU6050_RA_MEM_R_W, chunkSize, progBuffer);

    // verify data if needed
    if (verify && verifyBuffer) {
      MPUSetMemoryBank(bank);
      MPUSetMemoryStartAddress(address);
      i2c_readbytes(MPU6050_I2C_ADDR, MPU6050_RA_MEM_R_W, chunkSize, verifyBuffer);
      if (memcmp(progBuffer, verifyBuffer, chunkSize) != 0) {
        free(verifyBuffer);
        if (useProgMem) free(progBuffer);
        return false; // uh oh.
      }
    }

    // increase byte index by [chunkSize]
    i += chunkSize;

    // uint8_t automatically wraps to 0 at 256
    address += chunkSize;

    // if we aren't done, update bank (if necessary) and address
    if (i < dataSize) {
      if (address == 0) bank++;
      MPUSetMemoryBank(bank);
      MPUSetMemoryStartAddress(address);
    }
  }
  if (verify) free(verifyBuffer);
  if (useProgMem) free(progBuffer);
  return true;
}



uint16_t merge_bytes( int LSB, int MSB) {
  return  (uint16_t) ((( LSB & 0xFF) << 8) | MSB);
}

// 16 bits data on the MPU6050 are in two registers,
// encoded in two complement. So we convert those to int16_t
int16_t two_complement_to_int( int LSB, int MSB) {
  int16_t signed_int = 0;
  uint16_t word;

  word = merge_bytes(LSB, MSB);

  if((word & 0x8000) == 0x8000) { // negative number
        signed_int = (int16_t) -(~word);
    } else {
        signed_int = (int16_t) (word & 0x7fff);
    }

    return signed_int;
}


/* #define MPU6050_I2C_ADDR 0x68 */

/*
  MPU6050 ADDRESSES

  See

  https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Datasheet1.pdf

  For usage
  */
#define REG_ACCEL_ZOUT_H 0x3F
#define REG_ACCEL_ZOUT_L 0x40
#define REG_PWR_MGMT_1 0x6B
#define REG_ACCEL_CONFIG 0x1C
#define REG_SMPRT_DIV 0x19
#define REG_CONFIG 0x1A
#define REG_FIFO_EN 0x23
#define REG_USER_CTRL 0x6A
#define REG_FIFO_COUNT_L 0x72
#define REG_FIFO_COUNT_H 0x73
#define REG_FIFO 0x74
#define REG_WHO_AM_I 0x75
#define REG_INT_ENAB 0x38
#define REG_INT_STAT 0x3A

#define PI 3.14159265

char accel_x_h,accel_x_l,accel_y_h,accel_y_l,accel_z_h,accel_z_l,temp_h,temp_l;
char gyro_x_h,gyro_x_l,gyro_y_h,gyro_y_l,gyro_z_h,gyro_z_l;
uint16_t fifo_len = 0;
int16_t x_accel = 0;
int16_t y_accel = 0;
int16_t z_accel = 0;
int16_t x_gyro = 0;
int16_t y_gyro = 0;
int16_t z_gyro = 0;
int16_t temp = 0;
float x_accel_g, y_accel_g, z_accel_g, temp_f;
float x_gyro_g, y_gyro_g, z_gyro_g;

void readfifo() {
  /*
    Data is written to the FIFO in order of register number (from
    lowest to highest). If all the FIFO enable flags (see below) are
    enabled and all External Sensor Data registers (Registers 73 to
    96) are associated with a Slave device, the contents of registers
    59 through 96 will be written in order at the Sample Rate.
  */

  accel_x_h = i2c_read(REG_FIFO); // Reg 59-64
  accel_x_l = i2c_read(REG_FIFO);
  accel_y_h = i2c_read(REG_FIFO);
  accel_y_l = i2c_read(REG_FIFO);
  accel_z_h = i2c_read(REG_FIFO);
  accel_z_l = i2c_read(REG_FIFO);

  gyro_x_h = i2c_read(REG_FIFO); // Reg 67-72
  gyro_x_l = i2c_read(REG_FIFO);
  gyro_y_h = i2c_read(REG_FIFO);
  gyro_y_l = i2c_read(REG_FIFO);
  gyro_z_h = i2c_read(REG_FIFO);
  gyro_z_l = i2c_read(REG_FIFO);

  x_accel = two_complement_to_int(accel_x_h, accel_x_l);
  x_accel_g = ((float)x_accel) / 16384; // AFS_SEL = 0

  y_accel = two_complement_to_int(accel_y_h, accel_y_l);
  y_accel_g = ((float)y_accel) / 16384;

  z_accel = two_complement_to_int(accel_z_h, accel_z_l);
  z_accel_g = ((float)z_accel) / 16384;

  x_gyro = two_complement_to_int(gyro_x_h, gyro_x_l);
  x_gyro_g = ((float)x_gyro) / 250; // FS_SEL = 0

  y_gyro = two_complement_to_int(gyro_y_h, gyro_y_l);
  y_gyro_g = ((float)y_gyro) / 250;

  z_gyro = two_complement_to_int(gyro_z_h, gyro_z_l);
  z_gyro_g = ((float)z_gyro) / 250;
}

int calcypr(double *data) {
    // yaw: (about Z axis)
     /* data[0] = atan2(2*q -> x*q -> y - 2*q -> w*q -> z, 2*q -> w*q -> w + 2*q -> x*q -> x - 1); */
    data[0] = 0;
    // pitch: (nose up/down, about Y axis)
    data[1] = atan2(x_accel_g, sqrt(y_accel_g*y_accel_g + z_accel_g*z_accel_g));
    // roll: (tilt left/right, about X axis)
    data[2] = atan2(y_accel_g , z_accel_g);
    if (z_accel_g < 0) {
        if(data[1] > 0) {
            data[1] = PI - data[1];
        } else {
            data[1] = -PI - data[1];
        }
    }
    return 0;
}

void stop(int signum)
{
   run = 0;
}

int main(int argc, char *argv[]) {

  int adapter_nr = 1;
  char bus_filename[250];

  snprintf(bus_filename, 250, "/dev/i2c-1", adapter_nr);
  file = open(bus_filename, O_RDWR);
  if (file < 0) {
    exit(1);
  }

  if (ioctl(file, I2C_SLAVE, MPU6050_I2C_ADDR) < 0) {
    exit(1);
  }

  /* MPU6040 SETUP */
  i2c_write(REG_PWR_MGMT_1, 0x01);
  i2c_write(REG_ACCEL_CONFIG, 0x00);
  i2c_write(REG_SMPRT_DIV, 0x07);
  /* i2c_write(REG_CONFIG, 0x01); // Enable digital low pass filter */
  i2c_write(REG_CONFIG, 0x11); // Enable digital low pass filter
  i2c_write(REG_FIFO_EN, 0x78); // Reg0x23 11110000
                                // We need gravity and gyro readings
  i2c_write(REG_USER_CTRL, 0x44);
  i2c_write(REG_INT_ENAB, 0x01); // Reg0x38 00000001
                                 // Enable interrupt status


  /* MPU NEW */
  /* mpu.initialize(); */
  /* mpu.dmpInitialize(); */
  /* mpu.CalibrateAccel(6); */
  /* mpu.CalibrateGyro(6); */
  /* mpu.setDMPEnabled(true); */


  if (writeMemoryBlock(dmpMemory, MPU6050_DMP_CODE_SIZE, 0, 0, true)) {
    printf("Success! DMP code written and verified.\n");
  } else {
    printf("FAILURE\n");
  }

  /* RUN */
  while (run) {

    /* int tcp_rx_buffer[296]; */
    /* const int nbBytesRead = client.read(tcpRxBuffer, 296); */
    /* for (int i = 0; i < nbBytesRead; i += 3) { */
    /*   ctrl_values[tcpRxBuffer[i + 1]] = tcpRxBuffer[i + 2]; */
    /* } */

    int ready = i2c_read(REG_INT_STAT);

    printf("%d ", ready & 1);

    if(!((ready & 1) == 1)){
      printf("NOT READY\n");
      /* sleep(1); */
      continue;
    }


    /* MPU NEW */

    /* int fifoBuffer[64]; */

    /* if(!(mpu.dmpGetCurrentFIFOPacket(fifoBuffer) == 1)){ */
    /*   printf("NOT READY\n"); */
    /*   /\* sleep(1); *\/ */
    /*   continue; */
    /* } */

    /* mpu.dmpGetQuaternion(&q, fifoBuffer); */
    /* mpu.dmpGetGravity(&gravity, &q); */
    /* mpu.dmpGetYawPitchRoll(ypr, &q, &gravity); */
    /* mpu.dmpGetGyro(&gy, fifoBuffer); */


    accel_x_h = i2c_read(REG_FIFO_COUNT_L);
    accel_x_l = i2c_read(REG_FIFO_COUNT_H);
    fifo_len = merge_bytes(accel_x_h, accel_x_l);

    /* printf("FIFO LEN:%d\n", fifo_len); */

    if (fifo_len == 1024) {
      /* printf("fifo overflow !\n"); */
      i2c_write(REG_USER_CTRL, 0x44);
      printf("FIFO OVERFLOW\n");
      continue;
    }

    if (fifo_len >= 12) {

      /* printf("Reading MPU\n\n"); */

      readfifo();

      double gyroarr[] = {-z_gyro_g, -y_gyro_g, x_gyro_g};

      double yprarr[3];

      calcypr(yprarr);

      printf("accel: x %.3fg   y %.3fg   z %.3fg\t", x_accel_g,
             y_accel_g, z_accel_g);
      printf("gyro: x %.3f°/s   y %.3f°/s   z %.3f°/s\t", x_gyro_g,
             y_gyro_g, z_gyro_g);
      printf("ypr: %.3f %.3f %.3f", yprarr[0], yprarr[1], yprarr[2]);

      printf("\n");

      usleep(10000);

    } else {
      usleep(10000);
    }
  }

  /* SHUTDOWN */
  printf("\n\nTidying up\n");

  return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

/* I2C + MPU6050 */
#include <linux/i2c-dev.h>
#include <stdint.h>

#include <errno.h>
#include <stdbool.h>

#include "i2c.h"


/*
  MPU6050 i2c reading adapted from:
  https://openest.io/en/services/mpu6050-accelerometer-on-raspberry-pi

  and

  https://github.com/richardghirst/PiBits/blob/master/MPU6050-Pi-Demo/I2Cdev.cpp
*/



int file = -1;


// Please note, this is not the recommanded way to write data
// to i2c devices from user space.
void i2c_write(__u8 reg_address, __u8 val) {
    char buf[2];
    if(file < 0) {
        printf("Error, i2c bus is not available\n");
        exit(1);
    }

    buf[0] = reg_address;
    buf[1] = val;

    if (write(file, buf, 2) != 2) {
        printf("Error, unable to write to i2c device\n");
        exit(1);
    }

}

// Please note, this is not thre recommanded way to read data
// from i2c devices from user space.
char i2c_read(uint8_t reg_address) {
    char buf[1];
    if(file < 0) {
        printf("Error, i2c bus is not available\n");
        exit(1);
    }

    buf[0] = reg_address;

    if (write(file, buf, 1) != 1) {
        printf("Error, unable to write to i2c device\n");
        exit(1);
    }


    if (read(file, buf, 1) != 1) {
        printf("Error, unable to read from i2c device\n");
        exit(1);
    }

    return buf[0];

}

int8_t i2c_readbit(uint8_t regAddr, uint8_t bitNum) {
    uint8_t b;
    b = i2c_read(regAddr);
    return b & (1 << bitNum);
}

void i2c_writebit(uint8_t regAddr, uint8_t bitNum, uint8_t data) {
    uint8_t b;
    b = i2c_read(regAddr);
    b = (data != 0) ? (b | (1 << bitNum)) : (b & ~(1 << bitNum));
    i2c_write(regAddr, b);
}

bool i2c_writebytes(uint8_t regAddr, uint8_t length, uint8_t* data) {
    int8_t count = 0;
    uint8_t buf[128];

    if (length > 127) {
        fprintf(stderr, "Byte write count (%d) > 127\n", length);
        return false;
    }

    buf[0] = regAddr;
    memcpy(buf+1,data,length);
    count = write(file, buf, length+1);
    if (count < 0) {
        fprintf(stderr, "Failed to write device(%d):\n", count);
        close(file);
        return false;
    } else if (count != length+1) {
        fprintf(stderr, "Short write to device, expected %d, got %d\n", length+1, count);
        close(file);
        return false;
    }

    return true;
}

int8_t i2c_readbytes(uint8_t regAddr, uint8_t length, uint8_t *data) {
    int8_t count = 0;

    if (write(file, &regAddr, 1) != 1) {
        fprintf(stderr, "Failed to write reg: %s\n", strerror(errno));
        close(file);
        return(-1);
    }
    count = read(file, data, length);
    if (count < 0) {
        fprintf(stderr, "Failed to read device(%d):\n", count);
        close(file);
        return(-1);
    } else if (count != length) {
        fprintf(stderr, "Short read to device, expected %d, got %d\n", length+1, count);
        close(file);
        return(-1);
    }

    return count;
}

void i2c_writebits(uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t data) {
  //      010 value to write
  // 76543210 bit numbers
  //    xxx   args: bitStart=4, length=3
  // 00011100 mask byte
  // 10101111 original value (sample)
  // 10100011 original & ~mask
  // 10101011 masked | value
  uint8_t b = i2c_read(regAddr);
  uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
  data <<= (bitStart - length + 1); // shift data into correct position
  data &= mask; // zero all non-important bits in data
  b &= ~(mask); // zero all important bits in existing byte
  b |= data; // combine data with existing byte
  i2c_write(regAddr, b);
}

int8_t i2c_readbits(uint8_t regAddr, uint8_t bitStart, uint8_t length) {
  // 01101001 read byte
  // 76543210 bit numbers
  //    xxx   args: bitStart=4, length=3
  //    010   masked
  //   -> 010 shifted
  uint8_t b = i2c_read(regAddr);
  uint8_t mask = ((1 << length) - 1) << (bitStart - length + 1);
  b &= mask;
  b >>= (bitStart - length + 1);
  return b;
}

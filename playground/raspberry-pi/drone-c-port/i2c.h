#ifndef I2C_H_
#define I2C_H_

int file;

void i2c_write(__u8 reg_address, __u8 val);

char i2c_read(uint8_t reg_address);

int8_t i2c_readbit(uint8_t regAddr, uint8_t bitNum);

void i2c_writebit(uint8_t regAddr, uint8_t bitNum, uint8_t data);

bool i2c_writebytes(uint8_t regAddr, uint8_t length, uint8_t* data);

int8_t i2c_readbytes(uint8_t regAddr, uint8_t length, uint8_t *data);

void i2c_writebits(uint8_t regAddr, uint8_t bitStart, uint8_t length, uint8_t data);

int8_t i2c_readbits(uint8_t regAddr, uint8_t bitStart, uint8_t length);

#endif // I2C_H_

/*
 * A drone using a raspberry pi zero using a PID controller (without the I for now)
 * To debug over wifi, connect to drone via its AP:
 *   nc 192.168.4.1 3000
 *
 * Set up the joystick to send via netcat (extract the channel and value positions, then send):
 *  cat  /dev/input/event20 | stdbuf -o0 xxd -c 24 -ps | stdbuf -o0 cut -c33,34,37,38,41,42 | stdbuf -o0 grep -v "^00" | stdbuf -o0 xxd -r -p | nc 192.168.4.1 3000
 *
 * Motor configuration
 * forward
 *  0(CW)  1(CCW)
 *  3(CCW) 2(CW)
 * back
 */

/* gcc darksidedrone.c -o darksidedrone -lm -lgsl -lgslcblas -Wall -pthread -lpigpio */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

/* GPIO */
#include <pigpio.h>

/* Server */
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>

#include <string.h>

/* Math */
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

/* I2C + MPU6050 */
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>

#include <stdbool.h>
#include "dmp.h"

const char ssid[] = "efdrobot";
const double Kp = 40; const double Kd = 0.8;
int armed = 0;

/* const int base_pw = 1000; */
/* const int base_pw = 650; */
const int base_pw = 900;

int throttle;

const int pins[] = {18,17,27,22};


/*
     (18)     (gyro)     (17)







     (22)                (27)
 */


int run=1;

int width = 1100;
int pin = 18;

int presocket, newsocket;
/* char buffer[1024]; */
char buffer[296];
struct sockaddr_in server;
struct sockaddr_storage serverutorage;
socklen_t addr_size;

double erroractarr[12] = {1, -1, -1, -1, -1, 1, 1, 1, 1, -1, 1, -1};

double ctrl_values[4] = {0, 0, 0, 0}; // Note: for yaw, joystick control
                                      // indicates relative change

double motorpwrarr[4];

/* const int gyroarr[3] = {-gy.z, -gy.y, gy.x}; */
/* double gyroarr[3]; */

double prevgyroarr[3] = {0, 0, 0};
double prevyprarr[3] = {0, 0, 0};

/* double yprarr[3]; */

void stop(int signum)
{
   run = 0;
}

int main(int argc, char *argv[]) {
  /* INITIALIZE MOTORS */
  if (gpioInitialise() < 0)
    return -1;

  gpioSetSignalFunc(SIGINT, stop);

  printf("\nMOTORINIT\n");

  for (int i = 0; i < 4; ++i) {
    printf("M%d, %d\n", pins[i], 1000);
    gpioServo(pins[i], 1000);
  }

  sleep(2);

  /* Test Motors */
  printf("\nMOTORTEST\n");

  for (int i = 0; i < 4; ++i) {
    printf("M%d, %d\n", pins[i], 1000);
    gpioServo(pins[i], 1200);
  }

  /* RUN */
  while (run) {
    if (armed != 1) {
      if (ctrl_values[0] < -100 && ctrl_values[1] < -100 &&
          ctrl_values[2] < -100 && ctrl_values[3] < -100) {
        armed = 1;
      }
    }

    armed = 1;
  }

  /* SHUTDOWN */
  printf("\n\nTidying up\n");

  for (int i = 0; i < 4; ++i) {
    gpioServo(pins[i], 1000); // 0 or 1000?
    printf("M%d, %d\n", pins[i], 1000);
  }

  gpioTerminate();

  return 0;
}

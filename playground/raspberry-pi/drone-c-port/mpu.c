#include <stdio.h>
#include <stdlib.h>

#include <string.h>

/* I2C + MPU6050 */
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <stdint.h>

#include <stdbool.h>

#include "mpu.h"
#include "i2c.h"

#define pgm_read_byte(p) (*(uint8_t *)(p))

void MPUSetMemoryBank(uint8_t bank, bool prefetchEnabled, bool userBank) {
  bank &= 0x1F;
  if (userBank) bank |= 0x20;
  if (prefetchEnabled) bank |= 0x40;
  i2c_write(MPU6050_RA_BANK_SEL, bank);
}

void MPUSetMemoryStartAddress(uint8_t address) {
  i2c_write(MPU6050_RA_MEM_START_ADDR, address);
}

bool MPUWriteMemoryBlock(const uint8_t *data, uint16_t dataSize, uint8_t bank, uint8_t address, bool verify) {
  uint8_t chunkSize, *verifyBuffer;
  uint8_t *progBuffer = NULL; // Keep compiler quiet
  uint16_t i; uint8_t j;

  MPUSetMemoryBank(bank, false, false);
  MPUSetMemoryStartAddress(address);

  bool useProgMem = true;

  if (verify) verifyBuffer = (uint8_t *)malloc(MPU6050_DMP_MEMORY_CHUNK_SIZE);
  if (useProgMem) progBuffer = (uint8_t *)malloc(MPU6050_DMP_MEMORY_CHUNK_SIZE);
  for (i = 0; i < dataSize;) {
    // determine correct chunk size according to bank position and data size
    chunkSize = MPU6050_DMP_MEMORY_CHUNK_SIZE;

    // make sure we don't go past the data size
    if (i + chunkSize > dataSize) chunkSize = dataSize - i;

    // make sure this chunk doesn't go past the bank boundary (256 bytes)
    if (chunkSize > 256 - address) chunkSize = 256 - address;

    if (useProgMem) {
      // write the chunk of data as specified
      for (j = 0; j < chunkSize; j++) progBuffer[j] = pgm_read_byte(data + i + j);
    } else {
      // write the chunk of data as specified
      progBuffer = (uint8_t *)data + i;
    }

    i2c_writebytes(MPU6050_RA_MEM_R_W, chunkSize, progBuffer);

    // verify data if needed
    if (verify && verifyBuffer) {
      MPUSetMemoryBank(bank, false, false);
      MPUSetMemoryStartAddress(address);
      i2c_readbytes(MPU6050_RA_MEM_R_W, chunkSize, verifyBuffer);
      if (memcmp(progBuffer, verifyBuffer, chunkSize) != 0) {
        free(verifyBuffer);
        if (useProgMem) free(progBuffer);
        return false; // uh oh.
      }
    }

    // increase byte index by [chunkSize]
    i += chunkSize;

    // uint8_t automatically wraps to 0 at 256
    address += chunkSize;

    // if we aren't done, update bank (if necessary) and address
    if (i < dataSize) {
      if (address == 0) bank++;
      MPUSetMemoryBank(bank, false, false);
      MPUSetMemoryStartAddress(address);
    }
  }
  if (verify) free(verifyBuffer);
  if (useProgMem) free(progBuffer);
  return true;
}

bool MPUWriteDMPConfig(const uint8_t *data, uint16_t dataSize) {
    uint8_t *progBuffer = NULL, success, special;
    uint16_t i, j;

    bool useProgMem = false;

    if (useProgMem) {
        progBuffer = (uint8_t *)malloc(8); // assume 8-byte blocks, realloc later if necessary
    }

    // config set data is a long string of blocks with the following structure:
    // [bank] [offset] [length] [byte[0], byte[1], ..., byte[length]]
    uint8_t bank, offset, length;
    for (i = 0; i < dataSize;) {
        if (useProgMem) {
            bank = pgm_read_byte(data + i++);
            offset = pgm_read_byte(data + i++);
            length = pgm_read_byte(data + i++);
        } else {
            bank = data[i++];
            offset = data[i++];
            length = data[i++];
        }

        if (length > 0) {
            if (useProgMem) {
                if (sizeof(progBuffer) < length) progBuffer = (uint8_t *)realloc(progBuffer, length);
                for (j = 0; j < length; j++) progBuffer[j] = pgm_read_byte(data + i + j);
            } else {
                progBuffer = (uint8_t *)data + i;
            }
            success = MPUWriteMemoryBlock(progBuffer, length, bank, offset, true);
            i += length;
        } else {
            if (useProgMem) {
                special = pgm_read_byte(data + i++);
            } else {
                special = data[i++];
            }
            if (special == 0x01) {
                i2c_write(REG_INT_ENAB, 0x32);  // single operation

                success = true;
            } else {
                // unknown special command
                success = false;
            }
        }

        if (!success) {
            if (useProgMem) free(progBuffer);
            return false;
        }
    }
    if (useProgMem) free(progBuffer);
    return true;
}

void MPUResetFIFO() {
  i2c_writebit(REG_USER_CTRL, 2, true);
}

void MPUSetFIFOEnabled(bool enabled) {
  i2c_writebit(REG_USER_CTRL, 6, enabled);
}

void MPUSetDMPEnabled(bool enabled) {
  i2c_writebit(REG_USER_CTRL, 7, enabled); // enable dmp
}

void MPUResetDMP() {
  i2c_writebit(REG_USER_CTRL, 3, true); // enable dmp
}

void MPUSetDMPConf1(uint8_t config) {
    i2c_write(MPU6050_RA_DMP_CFG_1, config);
}

void MPUSetDMPConf2(uint8_t config) {
    i2c_write(MPU6050_RA_DMP_CFG_2, config);
}

#ifndef MPU_H_
#define MPU_H_

/*
  MPU6050 ADDRESSES

  See

  https://invensense.tdk.com/wp-content/uploads/2015/02/MPU-6000-Datasheet1.pdf

  For usage
*/
#define REG_ACCEL_ZOUT_H              0x3F
#define REG_ACCEL_ZOUT_L              0x40
#define REG_PWR_MGMT_1                0x6B
#define REG_ACCEL_CONFIG              0x1C
#define REG_SMPRT_DIV                 0x19
#define REG_CONFIG                    0x1A
#define REG_FIFO_EN                   0x23
#define REG_USER_CTRL                 0x6A
#define REG_FIFO_COUNT_L              0x72
#define REG_FIFO_COUNT_H              0x73
#define REG_FIFO                      0x74
#define REG_WHO_AM_I                  0x75
#define REG_INT_ENAB                  0x38
#define REG_INT_STAT                  0x3A

#define MPU6050_I2C_ADDR              0x68
#define MPU6050_RA_BANK_SEL           0x6D
#define MPU6050_RA_MEM_START_ADDR     0x6E
#define MPU6050_RA_MEM_R_W            0x6F

#define MPU6050_DMP_MEMORY_CHUNK_SIZE 16

#define MPU6050_RA_DMP_CFG_1        0x70
#define MPU6050_RA_DMP_CFG_2        0x71

#define DMP_PACKET_SIZE               42
/* #define DMP_PACKET_SIZE               12 */

void MPUSetMemoryBank(uint8_t bank, bool prefetchEnabled, bool userBank);

void MPUSetMemoryStartAddress(uint8_t address);

bool MPUWriteMemoryBlock(const uint8_t *data, uint16_t dataSize, uint8_t bank, uint8_t address, bool verify);

bool MPUWriteDMPConfig(const uint8_t *data, uint16_t dataSize);

void MPUResetFIFO();

void MPUSetFIFOEnabled(bool enabled);

void MPUSetDMPEnabled(bool enabled);

void MPUResetDMP();

void MPUSetDMPConf1(uint8_t config);

void MPUSetDMPConf2(uint8_t config);

#endif // MPU_H_

/* gcc receive-test.c -o receive-test -lm -Wall -pthread */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

/* Server */
#include <sys/socket.h>
#include <arpa/inet.h> //inet_addr
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>

#include <string.h>


int presocket, newsocket;
struct sockaddr_in server;
struct sockaddr_storage serverutorage;
socklen_t addr_size;
const int escBasePulseWidth = 1000;
int8_t ctrlvalues[] = {0, 0, 0, 0};

int run=1;


void serverinit() {
  presocket = socket(PF_INET, SOCK_STREAM, 0);
  /* Set socket state to nonblocking, so drone doesnt freeze when no
   instructions are received. */
  fcntl(presocket, F_SETFL, O_NONBLOCK);

  server.sin_family = AF_INET;
  server.sin_port = htons(7891);
  server.sin_addr.s_addr = INADDR_ANY; // tcp socket @ localhost
  /* Zeropad address field */
  bzero(&(server.sin_zero), 8);

  if (0 == bind(presocket, (struct sockaddr *) &server, sizeof(server)))
    printf("Bound socket\n");
  else
    printf("Error: Socket bind failure\n");

  if (listen(presocket, 5) == 0)
    printf("Listening\n");
  else
    printf("Error\n");

  printf("Conn\n");
}


void stop(int signum)
{
   run = 0;
}

int main(int argc, char *argv[]) {

    serverinit();

    uint8_t buffer[296];
    int s;

    while (run) {

        /* Accept incoming connections */
        if (!newsocket || newsocket == -1 || s == 0) {
            addr_size = sizeof serverutorage;
            newsocket = accept(presocket, (struct sockaddr *)&serverutorage, &addr_size);
            fcntl(newsocket, F_SETFL, O_NONBLOCK);
        }

        /* Read incoming instruction */
        s = recv(newsocket, buffer, 296, 0);

        int runs = 0;

        if (!(s == 0 || s < 0)) {
            runs = 0;
            for (int i = 0; i < s; i += 3) {
                printf(">: %d %d\n", buffer[i + 1], buffer[i + 2]);
                ctrlvalues[buffer[i + 1]] = buffer[i + 2];
                runs++;
            }
            printf("Received: %s (size: %d)\n\n", buffer, s);
            printf("Ctrl Values: %d %d %d %d (runs %d)\n\n",
                   ctrlvalues[0], ctrlvalues[1], ctrlvalues[2], ctrlvalues[3], runs);
            memset(buffer, 0, sizeof(buffer));
        }

    }
    
    return 0;
}

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

#define THISUNIT 0
const int otherUnit = (THISUNIT + 1) % 2;
const int cePin = 8;
const int csPin = 7;
RF24 radio(cePin, csPin); //
const uint64_t addresses[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };
unsigned long timer = 0;

void setup() {
  Serial.begin(115200);
  radio.begin();
  radio.setRetries(15,15);
  radio.openReadingPipe(1,addresses[THISUNIT]);
  Serial.print("isChipConnected: ");
  Serial.println(radio.isChipConnected());
  radio.startListening();
}

void loop() {
    if (millis() - timer > 1000) {
        timer = millis();
        radio.stopListening();
        Serial.print("This device: ");
        Serial.println((long int)addresses[THISUNIT], HEX);
        Serial.print("Now sending ");
        Serial.println(timer);
        radio.openWritingPipe(otherUnit);
        delay(20);
        bool ok = radio.write(&timer, sizeof(unsigned long));
        Serial.println(ok? "time sent" : "failed to send time");
        radio.openReadingPipe(1,addresses[THISUNIT]);
        radio.startListening();
    }
    if(radio.available()) {
        unsigned long receivedTime;
        radio.read(&receivedTime, sizeof(unsigned long));
        Serial.println(receivedTime);
        delay(20);
    }
}

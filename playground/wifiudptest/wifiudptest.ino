/*
 * Testing the Uno Wifi Rev2's udp sending capabilities (in view of debugging over the air)
 * On the server side listen on UDP port say 9999 using
 *   nc -l -u -p 9999
 *
 * You should receive the string "hello world"
 */
#include <WiFiNINA.h>
#include <WiFiUdp.h>

const char ssid[] = "WiFimodem-A7B6";
const char password[] = "qjy4egn4kd";
const IPAddress serverIp(192,168,0,37);
const int serverPortNumber = 9999;
WiFiUDP udp;

void setup() {
    WiFi.begin(ssid, password);
    udp.begin(3000);
    udp.beginPacket(serverIp, serverPortNumber);
    char sensorData[] = "hello world";
    udp.write(sensorData, 11);
    udp.endPacket();
}

void loop() {}
